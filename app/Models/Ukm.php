<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ukm extends Model
{
    use HasFactory;

//    protected $with = ['users','tags','ukmTags','categories','galleries','staffs'];
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function users(){
        return $this->belongsTo(User::class,'leader_nrp','nrp');
    }

    public function tags(){
        return $this->belongsToMany(Tag::class,'ukm_tags','ukm_id','tag_id');
    }

    public function ukmTags(){
        return $this->hasMany(UkmTag::class);
    }

    public function categories(){
        return $this->belongsTo(Category::class,'category_id');
    }
    public function galleries(){
        return $this->hasMany(Gallery::class);
    }

    public function achievements ($id){
        $achievements = Achievement::whereUkmId($id)->get();
        return $achievements;
    }
    public function faqs(){
        return $this->hasMany(Faq::class);

    }
    public function questions(){
        return $this->hasMany(Question::class);

    }
    public function staffs(){
        return $this->hasMany(Staff::class);

    }
}
