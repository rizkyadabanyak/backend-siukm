<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    use HasFactory;
    protected $with = ['mahasiswa'];

    public function ukms(){

        return $this->hasMany(Ukm::class);
    }

    public function mahasiswa (){
        return $this->belongsTo(User::class,'nrp','nrp');

    }
}
