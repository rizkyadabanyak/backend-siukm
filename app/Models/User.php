<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $table = 'mahasiswas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function ukms(){
        return $this->hasOne(Ukm::class,'leader_nrp','nrp');
    }

    public function register(){
        return $this->hasMany(RegisForm::class);
    }
    public function roles(){
        return $this->belongsTo(Role::class,'role_id');
    }
    public function questions(){
        return $this->hasMany(Question::class);
    }
}
