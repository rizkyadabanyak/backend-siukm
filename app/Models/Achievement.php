<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Achievement extends Model
{
    use HasFactory;

    public function ukm(){
        return $this->belongsToMany(Ukm::class,'achievements');
    }
    public function galleries(){
        return $this->belongsTo(Gallery::class,'gallery_id');

    }

}
