<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RegisForm extends Model
{
    use HasFactory;
    protected $with = ['mahasiswa','ukms'];
    public function mahasiswa (){
        return $this->belongsTo(User::class,'nrp','nrp');

    }
    public function ukms (){
        return $this->belongsTo(Ukm::class,'ukm_id',);

    }
}
