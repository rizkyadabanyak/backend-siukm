<?php

namespace App\Http\Controllers\Layouts;

use App\Http\Controllers\Controller;
use App\Models\Faq;
use App\Models\Question;
use App\Models\Ukm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {

        if($request->ajax()) {
//        $data = Item::orderBy('created_at','DESC')->get();
            if (Auth::user()->role_id == 3)
            {
                $ukmId = Ukm::whereLeaderNrp(Auth::user()->nrp)->first();
                $data = Question::whereUkmId($ukmId->ukm_id)->get();

            }else{
                $data = Question::all();

            }

//        dd($data);
            return DataTables::of($data)
                ->addColumn('created_at', function($mhs){
                    $a = date('d-m-Y', strtotime($mhs->created_at));
                    return $a;
                })
                ->addColumn('action', function($data){
//                    $button = '<a href="'.route('admin.questions.show',$data->id).'" class="btn btn-info"><i class="fa fa-pencil"></i> Show</a>';
                    $button = '<a href="#" class="btn btn-info btn-show" id="btn-show" data-toggle="modal" data-target="#modal-detail" data-id="'.$data->id.'" data-value="'.$data->value.'" data-name="'.$data->mahasiswas->name.'" data-no="'.$data->mahasiswas->phone_number.'" data-email="'.$data->mahasiswas->email.'"><i class="fa fa-pencil"></i> Show</a>';

                    $button .= '<a  href="'.route('admin.questionsDestroy',$data->id).'" class="btn btn-danger" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-trash"></i> Delete</a>';
                    return $button;
                })
                ->rawColumns(['created_at','action'])
                ->make(true);
        }
        view()->share([
            'notif' => $this->notif()

        ]);
        return view('layouts.question.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Question::findOrFail($id);
        view()->share([
           'data' => $data
        ]);
        return view('layouts.question.show-hover');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Question::findOrFail($id);
        $data->delete();

        return redirect()->back()->with('danger','data has been deleted');
    }
}
