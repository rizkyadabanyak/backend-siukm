<?php

namespace App\Http\Controllers\Layouts;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Ukm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if($request->ajax()) {
//        $data = Item::orderBy('created_at','DESC')->get();
            $data = Category::all();

//        dd($data);
            return DataTables::of($data)
                ->addColumn('action', function($data){
                    $button = '<a href="categories/'.$data->id.'/edit'.'" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>';
                    $button .= '<a  href="'.route('admin.categoriesDestroy',$data->id).'" class="btn btn-danger" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-trash"></i> Delete</a>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        view()->share([
            'notif' => $this->notif()
        ]);
        return view('layouts.category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share([
            'notif' => $this->notif()
        ]);
        return view('layouts.category.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Category();

        $validates = [
            'name'  =>  'required|string',
            'dsc'   =>  'required'
        ];

        $request->validate($validates);

        $file = $request->file('image');

        $name = rand(999999,1);
        $extension = $file->getClientOriginalExtension();
        $newName = $name.'.'.$extension;
//                $path = $request->file('image')->storeAs('public',$newName);
        Storage::putFileAs('public',$file,$newName);

        $imgDB = 'storage/'.$newName;
        $data->logo = $imgDB;

        $data->name = $request->name;
        $data->slug = \Str::slug($request->name);
        $data->desc = $request->dsc;

        $data->save();

        return redirect()->route('admin.categories.index')->with('success','success add UKM');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Category::findOrFail($id);
        view()->share([
            'data' => $data,
                'notif' => $this->notif()
        ]);
        return view('layouts.category.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = Category::findOrFail($id);

        $validates = [
            'name'  =>  'required|string',
            'dsc'   =>  'required'
        ];

        $request->validate($validates);


        if ($request->file('image')){
            $file = $request->file('image');

            $name = rand(999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
//                $path = $request->file('image')->storeAs('public',$newName);
            Storage::putFileAs('public',$file,$newName);

            $imgDB = 'storage/'.$newName;
            $data->logo = $imgDB;
        }else{
            $data->logo = $request->newImage;
        }


        $data->name = $request->name;
        $data->slug = \Str::slug($request->name);
        $data->desc = $request->dsc;

        $data->save();

        return redirect()->route('admin.categories.index')->with('success','edit add UKM');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Category::findOrFail($id);
        $data->delete();

        return redirect()->back()->with('danger','data has been deleted');
    }
}
