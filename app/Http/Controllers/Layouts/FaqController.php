<?php

namespace App\Http\Controllers\Layouts;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Faq;
use App\Models\Ukm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {

        if($request->ajax()) {
//        $data = Item::orderBy('created_at','DESC')->get();
            if (Auth::user()->role_id == 3)
            {
                $ukmId = Ukm::whereLeaderNrp(Auth::user()->nrp)->first();
                $data = Faq::whereUkmId($ukmId->id)->get();
            }else{
                $data = Faq::all();

            }

//        dd($data);
            return DataTables::of($data)
                ->addColumn('ukm', function($data){
                    $a = $data->ukms->name;
                    return $a;
                })
                ->addColumn('action', function($data){
                    $button = '<a href="faqs/'.$data->id.'/edit'.'" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>';
                    $button .= '<a  href="'.route('admin.faqsDestroy',$data->id).'" class="btn btn-danger" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-trash"></i> Delete</a>';
                    return $button;
                })
                ->rawColumns(['ukm','action'])
                ->make(true);
        }
        view()->share([
            'notif' => $this->notif()
        ]);
        return view('layouts.faq.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ukms  = Ukm::all();

        view()->share([
           'ukms' => $ukms,
            'notif' => $this->notif()

        ]);
        return view('layouts.faq.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Faq();

        $validates = [
            'value'  =>  'required',
        ];

        $request->validate($validates);

        $data->ukm_id = $request->ukm;
        $data->nrp = Auth::user()->nrp;
        $data->value = $request->value;
        $data->answer = $request->answer;

        $data->save();

        return redirect()->route('admin.faqs.index')->with('success','success add faqs');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ukms  = Ukm::all();
        $data = Faq::findOrFail($id);
        view()->share([
            'data' => $data,
            'ukms' => $ukms,
            'notif' => $this->notif()
        ]);
        return view('layouts.faq.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Faq::findOrFail($id);

        $validates = [
            'value'  =>  'required',
        ];

        $request->validate($validates);

        $data->ukm_id = $request->ukm;
        $data->nrp = Auth::user()->nrp;
        $data->value = $request->value;
        $data->answer = $request->answer;

        $data->save();

        return redirect()->route('admin.faqs.index')->with('success','success edit faqs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Faq::findOrFail($id);
        $data->delete();

        return redirect()->back()->with('danger','data has been deleted');
    }
}
