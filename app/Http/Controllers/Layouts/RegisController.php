<?php

namespace App\Http\Controllers\Layouts;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Gallery;
use App\Models\RegisForm;
use App\Models\Ukm;
use App\Models\User;
use App\Models\ViewUkm;
use App\Models\ViewWeb;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class RegisController extends Controller
{
    public function actionFilterWeb(Request  $request){

        return redirect()->route('admin.filterUkm',[$request->id,$request->year]);
    }

    public function filterUkm($id,$year,Request $request){
        $mowyear = Carbon::now()->format('Y');
        $data = DB::select(DB::raw("select count(*) as total,ukm_id FROM regis_forms GROUP BY ukm_id"));
        $ukms = Ukm::where('name', '!=', null)->get();

//        dd($data);
        $tmp[] = "";
        $a = 0;
        $b = 0;
        foreach ($data as $list) {
            $tmp[$a++] = $list->total . ',';
        }

        $nama[] = "";
        foreach ($data as $list) {
            foreach ($ukms as $ukm) {
                if ($ukm->id == $list->ukm_id) {
                    $nama[$b++] = $ukm->name . ',';
                }

            }
        }

        if ($request->ajax()) {
            if (Auth::user()->role_id == 3) {
                $ukmID = Ukm::whereLeaderNrp(Auth::user()->nrp)->first();
                $mhs = RegisForm::whereUkmId($ukmID->id)->get();
            } else {
                $mhs = RegisForm::all();
            }


            return DataTables::of($mhs)
                ->addColumn('name', function ($mhs) {
                    $a = $mhs->mahasiswa->name;
                    return $a;
                })->addColumn('created_at', function ($mhs) {
                    $a = date('d-m-Y', strtotime($mhs->created_at));
                    return $a;
                })
                ->addColumn('ukm_id', function ($mhs) {
                    $a = $mhs->ukms->name;
                    return $a;
                })
                ->addColumn('action', function ($mhs) {
                    $button = '<a href="statistika-pendaftaran/detail/' . $mhs->id . '/show' . '" class="btn btn-info"><i class="fa fa-pencil"></i> Show</a>';
                    return $button;
                })
                ->rawColumns(['name', 'created_at', 'ukm_id', 'action'])
                ->make(true);
        }

        $viewWebs = ViewWeb::where('year', '=', $year)->orderBy('month', 'asc')->get();
        $avrgViewWeb = $this->statisticWeb($year);

        $tmpUkm = Ukm::where('name', '!=', null)->first();
        if ($tmpUkm == null) {
            $viewUkms = null;
            $avrgViewUkm = null;
            $viewWebYears = null;
        } else {
            $viewUkms = ViewUkm::where([
                'year' => $year,
                'ukm_id' => $tmpUkm->id
            ])->orderBy('month', 'asc')->get();
            $avrgViewUkm = $this->statisticUkm($tmpUkm->id, $year);

            $viewWebYears = ViewWeb::distinct()->get('year');
        }
        $viewUkmsYears = ViewUkm::whereUkmId($tmpUkm->id)->distinct()->get('year');

        $data = $this->statisticUkm($id,$year);

        if ($year==null){

        }else{
            $data = $this->statisticUkm($id,$year);

        }

        view()->share([
            'selctUkm' => $tmpUkm,
            'total' => $tmp,
            'nama' => $nama,
            'ukms' => $ukms,
            'viewWebs' => $viewWebs,
            'avrgViewWeb' => $avrgViewWeb,
            'viewUkms' => $viewUkms,
            'avrgViewUkm' => $avrgViewUkm,
            'viewWebYears' => $viewWebYears,
            'viewUkmsYears' => $viewUkmsYears,
            'notif' => $this->notif()
        ]);
        return view('layouts.statistika-pendaftaran.filter-ukm');
    }


    public function statisticWeb($year)
    {
        $viewWebs = ViewWeb::where('year', '=', $year)->orderBy('month', 'asc')->get();
        $viewWeb = 0;
        foreach ($viewWebs as $data) {
            $viewWeb = $viewWeb + $data->view;
        }

        $countViewWebs = ViewWeb::where('year', '=', $year)->count();
        if ($viewWeb == 0) {
            return 0;
        }
        $avrgViewWeb = ceil($viewWeb / $countViewWebs);
        return $avrgViewWeb;
    }

    public function statisticUkm($id, $year)
    {

        $viewUkms = ViewUkm::where([
            'year' => $year,
            'ukm_id' => $id
        ])->orderBy('month', 'asc')->get();

        $countViewUkms = ViewUkm::where([
            'year' => $year,
            'ukm_id' => $id
        ])->count();

        $viewUkm = 0;
        foreach ($viewUkms as $data) {
            $viewUkm = $viewUkm + $data->view;
        }

        if ($viewUkm == 0) {
            return 0;
        }


        $avrgViewUkm = ceil($viewUkm / $countViewUkms);
        return $avrgViewUkm;

    }

    public function pendaftaran(Request $request)
    {
        $data = DB::select(DB::raw("select count(*) as total,ukm_id FROM regis_forms GROUP BY ukm_id"));
        $ukms = Ukm::where('name', '!=', null)->get();

//        dd($data);
        $tmp[] = "";
        $a = 0;
        $b = 0;
        foreach ($data as $list) {
            $tmp[$a++] = $list->total . ',';
        }
//        dd($tmp);

        $nama[] = "";
        foreach ($data as $list) {
            foreach ($ukms as $ukm) {
                if ($ukm->id == $list->ukm_id) {
                    $nama[$b++] = $ukm->name . ',';
                }

            }
        }

        if ($request->ajax()) {
//        $data = Item::orderBy('created_at','DESC')->get();

            if (Auth::user()->role_id == 3) {
                $ukmID = Ukm::whereLeaderNrp(Auth::user()->nrp)->first();
                $mhs = RegisForm::whereUkmId($ukmID->id)->get();
            } else {
                $mhs = RegisForm::all();
            }


//        dd($data);
            return DataTables::of($mhs)
                ->addColumn('name', function ($mhs) {
                    $a = $mhs->mahasiswa->name;
                    return $a;
                })->addColumn('created_at', function ($mhs) {
                    $a = date('d-m-Y', strtotime($mhs->created_at));
                    return $a;
                })
                ->addColumn('ukm_id', function ($mhs) {
                    $a = $mhs->ukms->name;
                    return $a;
                })
                ->addColumn('action', function ($mhs) {
                    $button = '<a href="statistika-pendaftaran/detail/' . $mhs->id . '/show' . '" class="btn btn-info"><i class="fa fa-pencil"></i> Show</a>';
                    return $button;
                })
                ->rawColumns(['name', 'created_at', 'ukm_id', 'action'])
                ->make(true);
        }
        $year = Carbon::now()->format('Y');

        $viewWebs = ViewWeb::where('year', '=', $year)->orderBy('month', 'asc')->get();
        $avrgViewWeb = $this->statisticWeb($year);

        $tmpUkm = Ukm::where('name', '!=', null)->first();
        if ($tmpUkm == null) {
            $viewUkms = null;
            $avrgViewUkm = null;
            $viewWebYears = null;
        } else {
            $viewUkms = ViewUkm::where([
                'year' => $year,
                'ukm_id' => $tmpUkm->id
            ])->orderBy('month', 'asc')->get();
            $avrgViewUkm = $this->statisticUkm($tmpUkm->id, $year);

            $viewWebYears = ViewWeb::distinct()->get('year');
        }
        $viewUkmsYears = ViewUkm::whereUkmId($tmpUkm->id)->distinct()->get('year');

        view()->share([
            'notif' => $this->notif(),
            'selctUkm' => $tmpUkm,
            'total' => $tmp,
            'nama' => $nama,
            'ukms' => $ukms,
            'viewWebs' => $viewWebs,
            'avrgViewWeb' => $avrgViewWeb,
            'viewUkms' => $viewUkms,
            'avrgViewUkm' => $avrgViewUkm,
            'viewWebYears' => $viewWebYears,
            'viewUkmsYears' => $viewUkmsYears
        ]);
        return view('layouts.statistika-pendaftaran.index');
    }


    public function show($id)
    {
//
        $data = RegisForm::find($id);
        view()->share([
            'data' => $data,
            'notif' => $this->notif(),

        ]);
        return view('layouts.statistika-pendaftaran.show');

    }

    public function filter(Request $request, $id)
    {

        $data = DB::select(DB::raw("select count(*) as total,ukm_id FROM regis_forms GROUP BY ukm_id"));
        $ukms = Ukm::where('name', '!=', null)->get();

//        dd($data);
        $tmp[] = "";
        $a = 0;
        $b = 0;
        foreach ($data as $list) {
            $tmp[$a++] = $list->total . ',';
        }
//        dd($tmp);

        $nama[] = "";
        foreach ($data as $list) {
            foreach ($ukms as $ukm) {
                if ($ukm->id == $list->ukm_id) {
                    $nama[$b++] = $ukm->name . ',';
                }

            }
        }

        if ($request->ajax()) {
//        $data = Item::orderBy('created_at','DESC')->get();

            $mhs = RegisForm::whereUkmId($id)->get();

//        dd($data);
            return DataTables::of($mhs)
                ->addColumn('name', function ($mhs) {
                    $a = $mhs->mahasiswa->name;
                    return $a;
                })->addColumn('created_at', function ($mhs) {
                    $a = date('d-m-Y', strtotime($mhs->created_at));
                    return $a;
                })
                ->addColumn('ukm_id', function ($mhs) {
                    $a = $mhs->ukms->name;
                    return $a;
                })
                ->addColumn('action', function ($mhs) {
                    $button = '<a href="statistika-pendaftaran/detail/' . $mhs->id . '/show' . '" class="btn btn-info"><i class="fa fa-pencil"></i> Show</a>';
                    return $button;
                })
                ->rawColumns(['name', 'created_at', 'ukm_id', 'action'])
                ->make(true);
        }

        view()->share([
            'total' => $tmp,
            'nama' => $nama,
            'ukms' => $ukms,
            'id' => $id,
            'notif' => $this->notif(),

        ]);
//        dd($request->filter);
        return view('layouts.statistika-pendaftaran.filter');
    }


    public function filterWeb(Request $request, $yearFiler)
    {
        $data = DB::select(DB::raw("select count(*) as total,ukm_id FROM regis_forms GROUP BY ukm_id"));
        $ukms = Ukm::where('name', '!=', null)->get();

//        dd($data);
        $tmp[] = "";
        $a = 0;
        $b = 0;
        foreach ($data as $list) {
            $tmp[$a++] = $list->total . ',';
        }
//        dd($tmp);

        $nama[] = "";
        foreach ($data as $list) {
            foreach ($ukms as $ukm) {
                if ($ukm->id == $list->ukm_id) {
                    $nama[$b++] = $ukm->name . ',';
                }

            }
        }

        if ($request->ajax()) {
//        $data = Item::orderBy('created_at','DESC')->get();

            if (Auth::user()->role_id == 3) {
                $ukmID = Ukm::whereLeaderNrp(Auth::user()->nrp)->first();
                $mhs = RegisForm::whereUkmId($ukmID->id)->get();
            } else {
                $mhs = RegisForm::all();
            }


//        dd($data);
            return DataTables::of($mhs)
                ->addColumn('name', function ($mhs) {
                    $a = $mhs->mahasiswa->name;
                    return $a;
                })->addColumn('created_at', function ($mhs) {
                    $a = date('d-m-Y', strtotime($mhs->created_at));
                    return $a;
                })
                ->addColumn('ukm_id', function ($mhs) {
                    $a = $mhs->ukms->name;
                    return $a;
                })
                ->addColumn('action', function ($mhs) {
                    $button = '<a href="statistika-pendaftaran/detail/' . $mhs->id . '/show' . '" class="btn btn-info"><i class="fa fa-pencil"></i> Show</a>';
                    return $button;
                })
                ->rawColumns(['name', 'created_at', 'ukm_id', 'action'])
                ->make(true);
        }
        $year = Carbon::now()->format('Y');

        $viewWebs = ViewWeb::where('year', '=', $yearFiler)->orderBy('month', 'asc')->get();
        $avrgViewWeb = $this->statisticWeb($yearFiler);

        $tmpUkm = Ukm::where('name', '!=', null)->first();

        $viewUkms = ViewUkm::where([
            'year' => $year,
            'ukm_id' => $tmpUkm->id
        ])->orderBy('month', 'asc')->get();

        $avrgViewUkm = $this->statisticUkm($tmpUkm->id, $year);

        $viewWebYears = ViewWeb::distinct()->get('year');
        $viewUkmsYears = ViewUkm::whereUkmId($tmpUkm->id)->distinct()->get('year');

        view()->share([
            'selctUkm' => $tmpUkm,
            'total' => $tmp,
            'nama' => $nama,
            'ukms' => $ukms,
            'viewWebs' => $viewWebs,
            'avrgViewWeb' => $avrgViewWeb,
            'viewUkms' => $viewUkms,
            'avrgViewUkm' => $avrgViewUkm,
            'viewWebYears' => $viewWebYears,
            'viewUkmsYears' => $viewUkmsYears,
            'notif' => $this->notif(),
        ]);
        return view('layouts.statistika-pendaftaran.filter-year');
    }

}
