<?php

namespace App\Http\Controllers\Layouts;

use App\Http\Controllers\Controller;
use App\Models\Carousel;
use App\Models\Category;
use App\Models\Question;
use App\Models\Ukm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

class CarouselsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        if($request->ajax()) {
//        $data = Item::orderBy('created_at','DESC')->get();
            $data = Carousel::all();

//        dd($data);
            return DataTables::of($data)->addColumn('img', function($data){
//                $img = '<img src="'.asset($data->img).'">';
                $img = '<img src="'.asset($data->img).'" class="img-fluid img-thumbnail">';
                return $img;
            })
                ->addColumn('action', function($data){
                    $button = '<a href="carousels/'.$data->id.'/edit'.'" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>';
                    $button .= '<a  href="'.route('admin.carouselsDestroy',$data->id).'" class="btn btn-danger" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-trash"></i> Delete</a>';
                    return $button;
                })
                ->rawColumns(['img','action'])
                ->make(true);
        }

        view()->share([
            'notif' => $this->notif()
        ]);
        return view('layouts.carousels.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share([
            'notif' => $this->notif()
        ]);
        return view('layouts.carousels.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Carousel();
        $validates = [
            'image'      => 'required',
            'dsc'      => 'required',
        ];
        $request->validate($validates);

        $file = $request->file('image');

        $name = rand(999999,1);
        $extension = $file->getClientOriginalExtension();
        $newName = $name.'.'.$extension;
//                $path = $request->file('image')->storeAs('public',$newName);
        Storage::putFileAs('public',$file,$newName);

        $imgDB = 'storage/'.$newName;
        $data->img = $imgDB;
        $data->dsc = $request->dsc;

        $data->save();

        return redirect()->route('admin.carousels.index')->withSuccess('berhasil ditambah');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Carousel::find($id);
        view()->share([
            'data' => $data,
            'notif' => $this->notif()

        ]);

        return view('layouts.carousels.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Carousel::find($id);

        if ($file = $request->file('image') == null){
            $data->img = $request->newImage;
        }else{
            $file = $request->file('image');

            $name = rand(999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
//                $path = $request->file('image')->storeAs('public',$newName);
            Storage::putFileAs('public',$file,$newName);

            $imgDB = 'storage/'.$newName;
            $data->img = $imgDB;
//            dd($imgDB);
        }


        $data->dsc = $request->dsc;
        $data->save();
        return redirect()->route('admin.carousels.index')->withSuccess('berhasil di update');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Carousel::findOrFail($id);
        $data->delete();

        return redirect()->back()->with('danger','data has been deleted');
    }
}
