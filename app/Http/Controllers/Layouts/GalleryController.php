<?php

namespace App\Http\Controllers\Layouts;

use App\Http\Controllers\Controller;
use App\Models\Achievement;
use App\Models\Gallery;
use App\Models\Ukm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GalleryController extends Controller
{
    public function index($id)
    {
        $data = Ukm::find($id);
        $images = Gallery::whereUkmId($id)->get() ;
        $achievements = Achievement::whereUkmId($id)->get();


        view()->share([
            'data' => $data,
            'images' => $images,
            'achievements'=> $achievements,
            'notif' => $this->notif()


        ]);
        return view('layouts.ukm.gallery');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request,$id)
    {
//        dd($request->hasFile('image'));
        if ($request->hasFile('image'))
        {
            $files = $request->file('image');
            foreach ($files as $file){
                $data = new Gallery();

                $name = rand(99999,1);
                $extension = $file->getClientOriginalExtension();
                $newName = $name.'.'.$extension;
//                $path = $request->file('image')->storeAs('public',$newName);
                Storage::putFileAs('public',$file,$newName);

                $imgDB = 'storage/'.$newName;
                $data->ukm_id = $id;
                $data->name = $imgDB;
                $data->format = $extension;
                $data->desc = $imgDB;

                $data->save();

            }

        }
        return redirect()->back()->with('success','success upload image');
    }
    public function image(Request $request){
        $data = Gallery::find($request->thumbnail);

        if ($request->tmp != $request->tshumbnail && $request->tmp != null){
            $tmpData = Gallery::find($request->tmp);
//            dd($tmpData);
            $tmpData->thumbnail = '0';

            $tmpData->save();
        }

        $data->thumbnail = '1';

//        dd($data);
        $data->save();

        return redirect()->back()->with('success','success set thumbnail');
    }

    public function selectDelete (Request $request){
        $data =$request->images;

        Gallery::whereIn('id',$data)->delete();

//        return response()->json(['success',"yey"]);
        return redirect()->back()->with('success','success delete images');

    }

    public function achievements(Request $request,$id){

        $data = new Achievement();
        $data->ukm_id = $id;
        $data->gallery_id = $request->thumbnail;
        $data->name = $request->name;
        $data->position = $request->position;
        $data->desc = $request->desc;

//        dd($data);
        $data->save();

        return redirect()->back()->with('success','success set thumbnail');
    }
    public function destroyAchievements ($ukm_id,$gallery_id){
        $achievement = Achievement::where([
            'ukm_id' => $ukm_id,
            'gallery_id' => $gallery_id
        ])->delete();

        return redirect()->back()->with('danger','berhasil di hapus');

    }

}
