<?php

namespace App\Http\Controllers\Layouts;

use App\Http\Controllers\Controller;
use App\Models\Ukm;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\DataTables;

class LeaderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            //        $data = Item::orderBy('created_at','DESC')->get();
            $data = User::whereLeadUkm('1')->get();
            //        dd($data);
            return DataTables::of($data)
                ->addColumn('ukm', function($data){
                    if ($data->lead_ukm == 1) {
                        $ukm = Ukm::whereLeaderNrp($data->nrp)->first();
                        $a = $ukm->name;
                        return $a;
                    }
                })
            ->addColumn('action', function($data){
                $ukm= Ukm::whereLeaderNrp($data->nrp)->first();
                $button = '<a href="leader/'.$ukm->id.'/edit'.'" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>';
                $button .= '<a href="'.route('admin.leaderDestroy',$data->id).'" class="btn btn-danger" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-trash"></i> Delete</a>';
                return $button;
            })
            ->rawColumns(['ukm','action'])
            ->make(true);
        }
        view()->share([
            'notif' => $this->notif()

        ]);
            return view('layouts.leader.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mahasiswas = User::whereLeadUkm('0')->get();
        $selects = Ukm::whereLeaderNrp(null)->get();

        view()->share([
            'notif' => $this->notif(),
            'mahasiswas'=>$mahasiswas,
            'selects'=>$selects
        ]);
        return view('layouts/leader/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Ukm::find($request->select);
        $data->password = Hash::make($request->password);
        $data->leader_nrp = $request->nrp;

        $data->save();

        $mhs = User::whereNrp($request->nrp)->first();


        $mhs->role_id = '3';
        $mhs->lead_ukm = '1';
        $mhs->save();

        return redirect()->route('admin.leader.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mahasiswas = User::whereLeadUkm('0')->get();
        $selects = Ukm::all();
        $data = Ukm::find($id);
//        dd($mahasiswas);
        view()->share([
            'data'=>$data,
            'mahasiswas'=>$mahasiswas,
            'selects'=>$selects,
            'notif' => $this->notif()
        ]);
        return view('layouts.leader.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->select == $id){
            $data = Ukm::find($id);
        }else{
            $dataOld = Ukm::find($id);

            if ($request->nrp == $request->nrpOld) {

            }else{
                $ketua = User::whereNrp($dataOld->leader_nrp)->first();
                $ketua->lead_ukm = '0';
                $ketua->save();
            }
//            $ketua = User::whereNrp($dataOld->leader_nrp)->first();
//            $ketua->lead_ukm = '0';
//            $ketua->save();

            $dataOld->leader_nrp = null;
            $dataOld->save();

            $data = Ukm::find($request->select);
        }

        $data->leader_nrp = $request->nrp;
        if ($request->password){
            $data->password = Hash::make($request->password);
        }
        $data->save();

        $mhsOld = User::whereNrp($request->nrpOld)->first();
        $mhsOld->lead_ukm = '0';
        $mhsOld->save();

        $mhs = User::whereNrp($request->nrp)->first();
        $mhs->lead_ukm = '1';
        $mhs->save();



        return redirect()->route('admin.leader.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Ukm::find($id);
        $mhs = User::whereNrp($data->leader_nrp)->first();

        $mhs->lead_ukm = '0';
        $data->leader_nrp = null;
        $data->password = null;

//        dd($data);

        $mhs->save();
        $data->save();
        return redirect()->route('admin.leader.index');

    }
}
