<?php

namespace App\Http\Controllers\Layouts;

use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Models\User;
use Yajra\DataTables\DataTables;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        // $data = User::all();

        //         dd($data);
        if($request->ajax()) {
//        $data = Item::orderBy('created_at','DESC')->get();
            $data = User::all();

//        dd($data);
            return DataTables::of($data)
                ->addColumn('role_id', function($data){
                    $a = $data->roles->name;
                    return $a;
                })
                ->addColumn('action', function($data){
                    $button = '<a href="mahasiswa/'.$data->id.'/edit'.'" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>';
                    $button .= '<a  href="'.route('admin.mahasiswaDestroy',$data->id).'" class="btn btn-danger" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-trash"></i> Delete</a>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        view()->share([
            'notif' => $this->notif()

        ]);
        return view('layouts.mahasiswa.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();

        view()->share([
           'roles' => $roles,
            'notif' => $this->notif()
        ]);
        return view('layouts.mahasiswa.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new User();

        $validates = [
            'nrp'   =>  'required',
            'role_id'   =>  'required',
            'name'  =>  'required',
            'phone_number'  =>  'required',
            'major'  =>  'required',
            'address'  =>  'required',
            'email'  =>  'required',
        ];

        $request->validate($validates);

        $data->nrp = $request->nrp;
        $data->role_id = $request->role_id;
        $data->name = $request->name;
        $data->phone_number = $request->phone_number;
        $data->major = $request->major;
        $data->address = $request->address;
        $data->email = $request->email;

        $data->save();

        return redirect()->route('admin.mahasiswa.index')->with('success','Mahasiswa created successfully!');    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::all();

        $data = User::findOrFail($id);
        view()->share([
            'data' => $data,
            'roles' => $roles,
                'notif' => $this->notif()
        ]);
        return view('layouts.mahasiswa.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = User::findOrFail($id);

        $validates = [
            'nrp'   =>  'required',
            'role_id'   =>  'required',
            'name'  =>  'required',
            'phone_number'  =>  'required',
            'major'  =>  'required',
            'address'  =>  'required',
            'email'  =>  'required',
        ];

        $request->validate($validates);

        $data->nrp = $request->nrp;
        $data->role_id = $request->role_id;
        $data->name = $request->name;
        $data->phone_number = $request->phone_number;
        $data->major = $request->major;
        $data->address = $request->address;
        $data->email = $request->email;

        $data->save();

        return redirect()->route('admin.mahasiswa.index')->with('success','Data edited successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = User::findOrFail($id);
        $data->delete();

        return redirect()->back()->with('danger','data has been deleted');
    }
}
