<?php

namespace App\Http\Controllers\Layouts;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Staff;
use App\Models\Tag;
use App\Models\Ukm;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

class UkmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    function check()
    {
        $mhsNrp = Auth::user()->nrp;
        $data = Ukm::whereLeaderNrp($mhsNrp)->whereFinal('0')->first();

        return $data;
    }

    public function index(Request $request)
    {

        if($request->ajax()) {
            if (Auth::user()->role_id == 3)
            {
                $data = Ukm::whereLeaderNrp(Auth::user()->nrp)->get();
//                $data = Ukm::get();

            }else{
                $data = Ukm::orderBy('updated_at','DESC')->get();

            }
            return DataTables::of($data)->addColumn('category_id', function($data){

                if ($data->category_id){
                    $a = $data->categories->name;
                    return $a;
                }
                return 'empty';
            })->addColumn('action', function($data){
                    $button = '<a href="ukm/'.$data->id.'/edit'.'" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>';

                    if ($data->final == '1'){
                        $button .= '<a href="'.route('admin.ukm.show',$data->id).'" class="btn btn-success"><i class="fa fa-pencil"></i> Show</a>';
                        $button .= '<a href="'.route('admin.galleries.index',$data->id).'" class="btn btn-warning"><i class="fa fa-pencil"></i> Galleries</a>';
                    }

                    if (Auth::user()->role_id == 3){

                    }else{
                        $button .= '<a  href="'.route('admin.ukmDestroy',$data->id).'" class="btn btn-danger" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-trash"></i> Delete</a>';

                    }
                    return $button;
                })
                ->rawColumns(['category_id','action'])
                ->make(true);
        }
        view()->share([
            'notif' => $this->notif()

        ]);
        return view('layouts.ukm.index');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::all();
        $categories = Category::all();
        $users = User::all();

        if (Auth::user()->role_id == '3'){
            $count = Ukm::whereLeaderNrp(Auth::user()->nrp)->count();
            $data = $this->check();
            $cekUkm = Ukm::whereLeaderNrp(Auth::user()->nrp)->first();

            if ($count >=1 && $cekUkm->final == '1'){
                return redirect()->back()->withDanger('anda sudah mempunyai 1 ukm yang terdaftar');
            }
        }else{
            $data = null;
        }

        view()->share([
            'tags' => $tags,
            'categories' => $categories,
            'data' => $data,
            'users' => $users,
            'notif' => $this->notif()

        ]);
        return view('layouts.ukm.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->role_id == 3){
            $data = $this->check();
        }else{
            $data = null;
        }
        $validates = [
            'name'      => 'required',
            'category_id'=> 'required',
            'tag'=> 'required',
            'schedule'=> 'required',
            'benefit'=> 'required',
            'vision'=> 'required',
            'dsc'       => 'required',

        ];
        $request->validate($validates);

        if ($data == null ){
            $data = new Ukm();
            $file = $request->file('image');

            $name = rand(999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
//                $path = $request->file('image')->storeAs('public',$newName);
            Storage::putFileAs('public',$file,$newName);

            $imgDB = 'storage/'.$newName;
            $data->logo = $imgDB;
        }else{
            $data->logo = $request->newImage;
        }

        $data->category_id = $request->category_id;
        $data->name = $request->name;
        $data->slug = \Str::slug($request->name);
        $data->schedule = $request->schedule;
        $data->benefit = $request->benefit;
        $data->vision = $request->vision;
        $data->mission = $request->mission;
        $data->phone_number = $request->phone_number;
        $data->ig = $request->ig;
        $data->line = $request->line;
        $data->desc = $request->dsc;
        $data->final = '0';

        $data->save();

        $tags = $request->tag;
        $data->tags()->sync($tags);
        if (Auth::user()->role_id == 3){
            return redirect()->route('admin.staff.create',$data->id);
        }
        return redirect()->route('admin.ukm.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Ukm::findOrFail($id);

        view()->share([
           'data' => $data,
            'notif' => $this->notif()
        ]);
        return view('layouts.ukm.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tags = Tag::all();
        $categories = Category::all();

        $data = Ukm::find($id);
        $users = User::all();
        $staffs = Staff::whereUkmId($id)->get();
        view()->share([
            'tags' => $tags,
            'categories' => $categories,
            'data' => $data,
            'users' => $users,
            'staffs' => $staffs,
            'notif' => $this->notif()

        ]);
        return view('layouts.ukm.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Ukm::find($id);

        $validates = [
            'name'      => 'required',
            'category_id'=> 'required',
            'schedule'=> 'required',
            'benefit'=> 'required',
            'vision'=> 'required',
            'dsc'       => 'required',

        ];
        $request->validate($validates);

        if ($data == null ){
            $data = new Ukm();
            $file = $request->file('image');

            $name = rand(999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
//                $path = $request->file('image')->storeAs('public',$newName);
            Storage::putFileAs('public',$file,$newName);

            $imgDB = 'storage/'.$newName;
            $data->logo = $imgDB;
        }else{
            $data->logo = $request->newImage;
        }
        $data->category_id = $request->category_id;
        $data->name = $request->name;
        $data->slug = \Str::slug($request->name);
        $data->schedule = $request->schedule;
        $data->benefit = $request->benefit;
        $data->vision = $request->vision;
        $data->mission = $request->mission;
        $data->phone_number = $request->phone_number;
        $data->ig = $request->ig;
        $data->line = $request->line;

        $data->desc = $request->dsc;
        $data->status = $request->status;
        $data->final = '1';



        $data->save();

        $tags = $request->tag;
        $data->tags()->sync($tags);
        if (Auth::user()->role_id == 3){
            return redirect()->route('admin.staff.create',$data->id);
        }
        return redirect()->route('admin.ukm.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = Ukm::findOrFail($id);
            $data->delete();
            return redirect()->route('admin.ukm.index')->with('danger','delete UKM success');

        }catch (\Exception $e){
//            return $e->getMessage();
            return redirect()->back()->with('warning','cant delete this data bcs  : ' . $e->getMessage());
        }


    }
}
