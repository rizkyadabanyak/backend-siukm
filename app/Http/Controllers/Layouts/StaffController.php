<?php

namespace App\Http\Controllers\Layouts;

use App\Http\Controllers\Controller;
use App\Models\Periode;
use App\Models\Staff;
use App\Models\StaffUkm;
use App\Models\Ukm;
use App\Models\User;
use Illuminate\Http\Request;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $data = Ukm::find($id);
        $users = User::all();

        view()->share([
            'id' => $id,
            'data' => $data,
            'users' => $users,
            'notif' => $this->notif()

        ]);

        return view('layouts.ukm.staff.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {

        $dataCount = count($request->position);

//        dd($request->name);
        $validates = [
            'start_date' => 'required',
            'end_date' => 'required'
        ];

//        dd($request->position);
        $request->validate($validates);
        $ukm = Ukm::find($id);

        for ($i =0 ; $i<$dataCount;$i++){

            if ($request->position[$i] == null || $request->nrp[$i] == null){
                return redirect()->back()->with('warning','there is an empty field');
            }

            $data = new Staff();
            $data->ukm_id = $id;
            $data->position = $request->position[$i];
            $data->nrp = $request->nrp[$i];
            $data->start_date = $request->start_date;
            $data->end_date = $request->end_date;
            $data->save();
//            dd($data);
        }
        if ($ukm->start_date == null ){

            $ukm->start_date = $request->start_date;
            $ukm->end_date = $request->end_date;
        }
        $ukm->final = '1';

        $ukm->save();


        return redirect()->route('admin.ukm.index')->with('success','success add UKM');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Staff::find($id);

        $data->delete();
        return redirect()->route('admin.ukm.index');
    }
}
