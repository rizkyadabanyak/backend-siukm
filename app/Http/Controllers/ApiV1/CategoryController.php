<?php

namespace App\Http\Controllers\ApiV1;

use App\Http\Controllers\Controller;
use App\Http\Resources\UkmCollection;
use App\Http\Resources\UkmResource;
use App\Models\Category;
use App\Models\Tag;
use App\Models\Ukm;
use App\Models\ViewWeb;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function categoriesnav(Request $request){
        if ($request->id){
            $data = Ukm::whereCategoryId($request->id)->whereFinal('1')->get();

        }else{
            $data = Category::all();
            $tags = Tag::all();
        }
        $tags = null;

//        return new UkmCollection($data);

        return response()->json([
            'data' => $data,
            'tags' => $tags
        ],200);

    }

    public function categories(Request $request){
        if ($request->id){
            $data = Ukm::whereCategoryId($request->id)->whereFinal('1')->get();

        }else{
            $data = Category::all();
            $tags = Tag::all();
        }
        $tags = null;

        $year = Carbon::now()->format('Y');
        $month= Carbon::now()->format('m');

        $cek= ViewWeb::where([
            'month' => $month,
            'year' => $year
        ])->first();

        if (!$cek){
            $newView = new ViewWeb();

            $newView->month = $month;
            $newView->year = $year;
            $newView->view = 1;

            $newView->save();
        }
        else{
            $cek->view = $cek->view+1;
            $cek->save();
        }


//        return new UkmCollection($data);

        return response()->json([
            'data' => $data,
            'tags' => $tags
        ],200);

    }
}
