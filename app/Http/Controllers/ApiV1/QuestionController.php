<?php

namespace App\Http\Controllers\ApiV1;

use App\Http\Controllers\Controller;
use App\Models\Question;
use App\Models\Suggestions;
use App\Models\User;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function create(Request $request,$id){
        $cek = User::whereNrp($request->nrp)->first();

        $validates = [
            'nrp' => 'numeric|required',
            'name' => 'required',
            'phone_number' => 'required',
            'value' => 'required',
            'answer' => 'required',

        ];

        $request->validate($validates);

        $nrp = null ;
        if ($cek == null){
            $mhs = new User();

            $mhs->nrp = $request->nrp;
            $mhs->role_id = '4';
            $mhs->email = $request->email;
            $mhs->name = $request->name;
            $mhs->phone_number = $request->phone_number;

            $mhs->save();

            $nrp = $request->nrp;
        }else{
            $nrp = $cek->nrp;
        }

        $data = new Question();

        $data->ukm_id = $id;
        $data->nrp = $nrp;
        $data->value = $request->value;
        $data->answer = $request->answer;

        $data->save();

        return response()->json([
            'type' => 'success',
            'message' => 'Yey berhasil tanya'
        ]);

    }
}
