<?php

namespace App\Http\Controllers\ApiV1;

use App\Http\Controllers\Controller;
use App\Models\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function index (){
        $data = Tag::all();

        return response()->json([
            'tags' => $data
        ]);
    }
}
