<?php

namespace App\Http\Controllers\ApiV1;

use App\Http\Controllers\Controller;
use App\Models\RegisForm;
use App\Models\User;
use Illuminate\Http\Request;

class RegisController extends Controller
{
    public function regis(Request $request,$id){
//        dd('dqwqwd');
        $cek = User::whereNrp($request->nrp)->first();

        $cekRegis = RegisForm::whereUkmId($id)->whereNrp($request->nrp)->first();

        if ($cekRegis != null){
            return response()->json([
                'type' => 'danger',
                'message' => 'anda sudah mendaftar ukm ini'
            ]);
        }
        $nrp = null ;
        if ($cek == null){
            $mhs = new User();
            $validates = [
                'nrp' => 'numeric|required',
                'name' => 'required',
                'phone_number' => 'required',
                'major' => 'required',
                'address' => 'required',
                'email' => 'email|unique:mahasiswas'
            ];

            $request->validate($validates);

            $mhs->nrp = $request->nrp;
            $mhs->role_id = '4';
            $mhs->name = $request->name;
            $mhs->phone_number = $request->phone_number;
            $mhs->major = $request->major;
            $mhs->address = $request->address;
            $mhs->email = $request->email;

            $mhs->save();

            $nrp = $request->nrp;
        }else{
            $nrp = $cek->nrp;
        }

        $data = new RegisForm();

        $data->ukm_id = $id;
        $data->nrp = $nrp;
        $data->reason = $request->reason;

        $data->save();

        return response()->json([
            'type' => 'success',
            'message' => 'Yey berhasil'
        ]);


    }
}
