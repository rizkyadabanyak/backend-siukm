<?php

namespace App\Http\Controllers\ApiV1;

use App\Http\Controllers\Controller;
use App\Models\Tag;
use App\Models\Ukm;
use Illuminate\Http\Request;

class RecomController extends Controller
{
    public function selectTags (Request $request){
        $tags = $request->tag;
        $selects = [];
        if ($tags != null){
            $i = 0;
            $array = -1;
//        dd($tags);
            foreach ($tags as $tag){
                $i++;
                $array++;
                $selects[$array] = [
                    'tag_id' => $tag
                ];
            }

            $data = Ukm::all();

            if ($selects != null ){
                $posts = \DB::table('ukm_tags');
                foreach ($selects as $select) {
                    $posts = $posts->orwhere('tag_id', $select['tag_id']);
                }
                $fix = $posts->distinct()->get(['ukm_id']);;

                return response()->json([
                    'recoms' => $fix,
                    'data' => $data
                ]);
            }
            return response()->json([
                'status' => 'danger',
                'message' => 'failed'
            ]);


        }
//        return redirect()->route('index');
    }


    public function index(){
        $selects = \Session::get('select');
        $data = Ukm::all();

        if ($selects != null ){
            $posts = \DB::table('ukm_tags');
            foreach ($selects as $select) {
                $posts = $posts->orwhere('tag_id', $select['tag_id']);
            }
            $fix = $posts->distinct()->get(['ukm_id']);;
            view()->share([
                'recoms' => $fix,
                'data' => $data
            ]);
            return view('mahasiswa.content.recom');
        }
        return redirect()->route('biasa');
    }
}
