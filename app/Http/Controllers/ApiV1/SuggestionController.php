<?php

namespace App\Http\Controllers\ApiV1;

use App\Http\Controllers\Controller;
use App\Models\RegisForm;
use App\Models\Suggestions;
use App\Models\User;
use Illuminate\Http\Request;

class SuggestionController extends Controller
{
    public function index (Request $request){
        $cek = User::whereNrp($request->nrp)->first();

        $validates = [
            'nrp' => 'numeric|required',
            'name' => 'required',
            'major' => 'required',
            'suggest' => 'required',
            'reason' => 'required',

        ];

        $request->validate($validates);

        $nrp = null ;
        if ($cek == null){

            $mhs = new User();
            $mhs->nrp = $request->nrp;
            $mhs->role_id = '4';
            $mhs->name = $request->name;
            $mhs->major = $request->major;
            $mhs->phone_number = '000';


            $mhs->save();

            $nrp = $request->nrp;
        }else{
            $nrp = $cek->nrp;
        }

        $data = new Suggestions();

        $data->nrp = $nrp;
        $data->suggest = $request->suggest;
        $data->reason = $request->reason;

        $data->save();

        return response()->json([
            'type' => 'success',
            'message' => 'Yey berhasil memberi saran'
        ]);
    }
}
