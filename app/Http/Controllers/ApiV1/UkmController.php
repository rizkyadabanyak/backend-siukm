<?php

namespace App\Http\Controllers\ApiV1;

use App\Http\Controllers\Controller;
use App\Http\Resources\UkmResource;
use App\Models\Achievement;
use App\Models\Carousel;
use App\Models\Category;
use App\Models\Faq;
use App\Models\Gallery;
use App\Models\Ukm;
use App\Models\ViewUkm;
use App\Models\ViewWeb;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Stevebauman\Location\Facades\Location;

class UkmController extends Controller
{
    public function search($name){
        $data = Ukm::where('name','like','%'.$name.'%')->get();

        return response()->json([
           'data' => $data
        ]);

    }
    public function detail(Ukm $slug,Request $request){
//        $data = Ukm::find($request->id);
//        $galleries = Gallery::whereUkmId($request->id)->get();
//        $achievements = Achievement::whereUkmId($request->id)->get();
//        $faqs = Faq::whereUkmId($request->id)->get();
//
//
//        return response()->json(    [
//            'data' => $data,
//            'galleries' => $galleries,
//            'achievements' => $achievements,
//            'faqs' => $faqs
//        ]);]
//        $data = Ukm::whereSlug($slug)->first();
//        $data->increment('view');
//        Ukm::find($request->id)->incr ement('view');
//
//        $count = new ViewUkm();
//        $count->ukm_id = $request->id;
//        $count->ip = $request->ip();
//        $count->save();


        $year = Carbon::now()->format('Y');
        $month= Carbon::now()->format('m');

        $cek= ViewUkm::where([
            'ukm_id' => $slug->id,
            'month' => $month,
            'month' => $month,
            'year' => $year
        ])->first();

        if (!$cek){
            $newView = new ViewUkm();

            $newView->ukm_id = $slug->id;
            $newView->month = $month;
            $newView->year = $year;
            $newView->view = 1;

            $newView->save();
        }
        else{
            $cek->view = $cek->view+1;
            $cek->save();
        }

        return new UkmResource($slug);
    }

    public function carousels(){
        $data =Carousel::all();
        return response()->json($data,200);
    }
}
