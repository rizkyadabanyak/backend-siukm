<?php

namespace App\Http\Controllers;

use App\Models\Question;
use App\Models\Ukm;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function notif(){
        if (Auth::user()->role_id == 3)
        {
            $ukmId = Ukm::whereLeaderNrp(Auth::user()->nrp)->first();
            $data = Question::whereUkmId($ukmId->id)->get();

        }else{
            $data = Question::all();
        }

        return $data;

    }
}
