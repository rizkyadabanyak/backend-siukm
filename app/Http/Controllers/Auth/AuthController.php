<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Ukm;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use MongoDB\Driver\Session;
use Validator;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function showFormRegister(){
        $data = User::all();
        view()->share([
            'data' => $data,
        ]);
        return view('auths.regis');
    }

    public function register(Request $request)
    {
//        dd(Hash::make($request->password));

        $data = new Ukm();

        $data->leader_nrp = $request->leader_nrp;
        $data->password = Hash::make($request->password);
        $data->start_date = $request->start_date;
        $data->end_date = $request->end_date;

        $data->save();

        return redirect()->back();
    }

    public function showFormLogin(){
        if (Auth::check()) {
            return redirect('/');
        }
        return view('auths.login');
    }

    public function login(Request $request){

        $email = User::whereEmail($request->email)->first();


        if ($email) {
            $pass = $email->ukms->password;

            if (Hash::check($request->password, $pass)) {

                Auth::loginUsingId($email->id, TRUE);
                return redirect('/');
            }
        } else {

            return redirect()->route('admin.formLogin')->withDanger('akun tidak terdaftar');
        }
    }
    public function logout(){
        Auth::logout();
        return redirect()->route('admin.formLogin');
    }
}
