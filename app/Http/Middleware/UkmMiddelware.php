<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class UkmMiddelware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $token = '$2y$10$5we5Z7yY.qCxKBcDF91Wv.2W1XBaFmfAUhTQKxSsgk6kWSgIiVigW';

        if ($request->token == $token){
            return $next($request);
        }else{
            return response()->json([
                'message' => 'error'
            ]);
        }
        return $next($request);

    }
}
