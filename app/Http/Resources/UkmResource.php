<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UkmResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
//        return parent::toArray($request);

        return [
            'id' => $this->id,
            'category_id' => $this->category_id,
            'leader_nrp' => $this->leader_nrp,
            'name' => $this->name,
            'logo' => $this->logo,
            'benefit' => $this->benefit,
            'mission' => $this->mission,
            'vision' => $this->vision,
            'schedule' => $this->schedule,
            'phone_number' => $this->phone_number,
            'ig' => $this->ig,
            'line' => $this->line,
            'final' => $this->final,
            'desc' => $this->desc,
            'status' => $this->status,
            'tags' => $this->tags,
            'categories' => $this->categories->name,
            'galleries' => $this->galleries,
            'achievements' => $this->achievements($this->id),
            'faqs' => $this->faqs,
            'questions' => $this->questions,
            'staffs' => $this->staffs
        ];
    }

    public function with($request)
    {
        return [
            'status' => 'success'
        ];
    }
}
