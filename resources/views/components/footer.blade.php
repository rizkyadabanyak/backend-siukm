<style>
    .footer-left{
        color: #ffffff;
        margin-top:14px;
        letter-spacing: 1px;
        padding-right:20px;
        font-size:13px;
    }
</style>

<div class="footer"> 
    <div class="footer-left d-flex">
        <div class="ml-auto p-2">Copyright &copy; 2021 Made with ❤️ by Team New Normal</div>
    </div>
</div>