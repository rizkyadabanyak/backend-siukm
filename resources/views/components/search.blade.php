"language": {
    "search": "Cari ",
    "lengthMenu": "Menampilkan _MENU_ Data",
    "info": "Menampilkan _START_ hingga _END_ dari _TOTAL_ data tersimpan",
    "infoEmpty": "Menampilkan 0 dari 0 data tersimpan",
    "emptyTable": "Data belum tersedia",
    "paginate": {
        "next": "Selanjutnya",
        "previous": "Sebelumnya"
    }
},
