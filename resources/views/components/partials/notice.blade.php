
@if (session('success'))
    <div class="alert alert-success">
        <p>{{ session('success') }}</p>
    </div>
@elseif(session('danger'))
    <div class="alert alert-danger">
        <p>{{ session('danger') }}</p>
    </div>
@elseif(session('warning'))
    <div class="alert alert-warning">
        <p>{{ session('warning') }}</p>
    </div>
@endif
