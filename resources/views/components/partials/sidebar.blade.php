@push('style')
@endpush
<style>
    .logo{
        width: 150px;
        margin-top: 25px;
    }

    .main-sidebar .sidebar-menu li.active a{
        color: #1E3D59;
        font-weight: 600;
        
    }

    .main-sidebar .sidebar-menu li ul.dropdown-menu li a:hover {
        color: #1E3D59;
        background-color: inherit;
    }
    
</style>

<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="index.html">
                <img class="logo" src={{asset('stisla/assets/img/logo.png')}}>
            </a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">SI-UKM</a>
        </div>
        <ul class="sidebar-menu" >
            <li class="menu-header" ></li> <br>
            <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span >Dashboard</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{route('admin.pendaftaran.index')}}">Statistika Pendaftaran</a></li>
                    <li><a class="nav-link" href="{{route('admin.faqs.index')}}">FAQ</a></li>
                    <li><a class="nav-link" href="{{route('admin.questions.index')}}">Pertanyaan</a></li>
                    <li><a class="nav-link" href="{{route('admin.suggestions.index')}}">Saran UKM</a></li>
                @if(Auth::user()->role_id == '2' || Auth::user()->role_id == '1')
                        <li><a class="nav-link" href="{{route('admin.leader.index')}}">Ketua UKM</a></li>
                    @endif
                </ul>
            </li>
            <li class="menu-header"></li>
            <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i> <span>UKM</span></a>
                <ul class="dropdown-menu">

                    <li><a class="nav-link" href="{{route('admin.ukm.index')}}">Daftar UKM</a></li>
                    @if(Auth::user()->role_id == '2' || Auth::user()->role_id == '1')
                        <li><a class="nav-link" href="{{route('admin.categories.index')}}">Kategori UKM</a></li>
                        <li><a class="nav-link" href="{{route('admin.tags.index')}}">Tags</a></li>
                        <li><a class="nav-link" href="{{route('admin.carousels.index')}}">Carousels</a></li>
                    @endif


                </ul>
            </li>
            @if(Auth::user()->role_id == '2' || Auth::user()->role_id == '1')
                <li class="menu-header"></li>

            <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i> <span>Mahasiswa</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{route('admin.mahasiswa.index')}}">Data Mahasiswa</a></li>

                </ul>
            </li>
            @endif
        </ul>
    </aside>
</div>
