{{--@component('layouts.app')--}}

{{--        <h1>ini login</h1>--}}

{{--@endcomponent--}}
@extends('auths.layouts.app')

@section('content')
<style>
    body{
        background-color:#E5E5E5!important;
    }
    .card-besar {
        margin-top: 80px;
    }
    .card{
        width:700px;
        background-color:#FFFFFF;
    }
    .button-masuk{
        width:200px;
        background-color:#FFAE00;
        border-color: #FFAE00;
    }
    .button-masuk:hover{
        background-color:#1E3D59;
        border-color: #1E3D59;
    }
    .navbar{
        width:100%;
        height:65px;
        background-color:#1E3D59;
    }
    .logo{
        width: 120px;
        margin-left: 15px;
    }
    .loginadmin{
        color:#FFFFFF;
        margin-right: 15px;
        margin-top: 11px;
        letter-spacing: 5px;
    }
    .footer{
        width:100%;
        height:65px;
        background-color:#1E3D59;
        position:absolute;
        bottom:0;
    }
    .card-header {
        background-color: rgba(0,0,0,.03);
        border-bottom: 0px solid rgba(0,0,0,.125);
    }
    .card-footer {
        background-color: rgba(0,0,0,.03);
        border-top: 0px solid rgba(0,0,0,.125);
    }
    body{
        min-height:100vh;
    }
    .semua{
        min-height:100vh;
        position:relative;
    }  
     
</style>
<div class="semua">
    <div class="navbar"> 
        <img class="logo" src={{asset('stisla/assets/img/logoputih.png')}}>
        <p class="loginadmin"><strong>LOGIN ADMIN</strong></p>
    </div>
    <div class="container ">
        <div class="card-besar d-flex justify-content-center">
            <div class="card">
                <div class="card-header">
                    <h3 class="text-center"><strong>L O G I N</strong></h3>
                </div>
                <x-partials.notice/>

                <form action="{{ route('admin.login') }}" method="post">
                    @csrf
                    <div class="card-body">
                        @if(session('errors'))
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                Something it's wrong:
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        @endif
                        @if (Session::has('error'))
                            <div class="alert alert-danger">
                                {{ Session::get('error') }}
                            </div>
                        @endif
                        <div class="form-group" style="letter-spacing: 1px; color: #000000">
                            <label for="">Email</label>
                            <input type="text" name="email" class="form-control" placeholder="Email">
                        </div>
                        <div class="form-group" style="letter-spacing: 1px; color: #000000">
                            <label for="">Password</label>
                            <input type="password" name="password" class="form-control" placeholder="Password">
                        </div>
                    </div>
                    <div class="card-footer">
                        <center>
                        <button type="submit" class="btn btn-primary btn-block button-masuk">M A S U K</button>
{{--                        <p class="text-center">Belum punya akun? <a href="{{ route('register') }}">Register</a> sekarang!</p>--}}
                        </center>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('components.footer')
</div>
@endsection