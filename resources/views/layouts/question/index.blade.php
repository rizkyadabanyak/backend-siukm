<x-app-layout >
    <style>
        .btn-primary, .btn-primary.disabled {
            box-shadow: 0 0 0 #FFC13B;
            background-color: #FFC13B;
            border-color: #FFC13B;
        }
        .btn-primary:active, .btn-primary:hover, .btn-primary.disabled:active, .btn-primary.disabled:hover {
            background-color: #1E3D59 !important;
        }
        .table-top{
            background-color: #1E3D59 !important;
            color: #FFFFFF;
        }
    </style>

    @push('style')
        <link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/datatables.min.css">
        <link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css">
        <style>
            /*.modal-backdrop{*/
            /*    position: relative;*/
            /*}*/
        </style>
    @endpush
        <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Daftar Pertanyaan</h1>
            </div>
            <x-partials.notice/>

            <div class="section-body">

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-width" id="item">
                                        <thead>
                                        <tr class="table-top">
                                                <th style="color: #ffffff">Pertanyaan</th>
                                                <th style="color: #ffffff">Tgl Submit</th>
                                                <th style="color: #ffffff">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>

                                </div>




                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

            <div class="modal fade" id="modal-detail" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Detail Pertanyaan</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Nama : <a id="name"></a></p>
                            <p>Email : <a id="email"></a></p>
                            <p>Whatsapp : <a id="phone_number"></a></p>
                            <p>Pertanyaan :<a id="value"></a> </p>

                        </div>
                        {{--                                            <div class="modal-footer">--}}
                        {{--                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>--}}
                        {{--                                                <button type="button" class="btn btn-primary">Save changes</button>--}}
                        {{--                                            </div>--}}
                    </div>
                </div>
            </div>
    </div>
        @push('script')
            <script type="text/javascript">
                $(document).ready(function(){
                    $(document).on('click', '.btn-show', function (event) {

                        let name = $(this).data('name');
                        let email = $(this).data('email');
                        let phone_number = $(this).data('phone_number');
                        let value = $(this).data('value');

                        $('#name').text(name);
                        $('#email').text(email);
                        $('#phone_number').text(phone_number);
                        $('#value').text(value);
                    });


                    $('#item').DataTable({
                        @include('components.search')
                        processing: true,
                        serverSide: true,
                        ajax: {
                            url: "{{route('admin.questions.index')}}",
                        },
                        columns: [
                            {
                                data: 'value',
                                name: 'value'
                            },
                            {
                                data: 'created_at',
                                name: 'created_at'
                            },
                            {
                                data: 'action',
                                name: 'action',
                                orderable: false
                            },

                        ]
                    });
                });
            </script>
        @endpush
</x-app-layout>

