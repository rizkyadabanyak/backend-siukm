<x-app-layout >
    @push('style')
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    @endpush
<div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Product</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Modules</a></div>
                    <div class="breadcrumb-item">DataTables</div>
                </div>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="container">
                                <div class="d-flex justify-content-between pt-5">
                                    <div>
                                        <h4>Question UKM</h4>

                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <h5 >Nama : {{$data->mahasiswas->name}}</h5>
                                <h5 >Email : {{$data->mahasiswas->email}}</h5>
                                <h5 >No Whatsapp : {{$data->mahasiswas->phone_number}}</h5>
                                <h5 >Pertanyaan : {{$data->value}}</h5>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</x-app-layout>
