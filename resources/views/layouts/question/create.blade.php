<x-app-layout >
    @push('style')
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    @endpush
<div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>UKM</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Modules</a></div>
                    <div class="breadcrumb-item">DataTables</div>
                </div>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="container">
                                <div class="d-flex justify-content-between pt-5">
                                    <div>
                                        <h4>Faq UKM</h4>

                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <form action="{{route('admin.faqs.store')}}" method="post">
                                    @csrf
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">UKM</label>
                                        <div class="col-sm-12 col-md-7">
                                            <select class="selectpicker" data-width="fit" data-size="5"  name="ukm" data-live-search="true">
                                                @foreach($ukms as $ukm)
                                                    <option value="{{$ukm->id}}" data-tokens="{{$ukm->name}}">{{$ukm->name}}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Pertanyaan</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="value" id="value" class="form-control @error('title') is-invalid @enderror">
                                            @error('title')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jawaban</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="answer" id="answer" class="form-control @error('title') is-invalid @enderror">
                                            @error('title')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button class="btn btn-primary">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</x-app-layout>
