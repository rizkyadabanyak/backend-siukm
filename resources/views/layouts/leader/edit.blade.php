<x-app-layout >

    @push('style')
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
        <style>
            .btn-primary, .btn-primary.disabled {
                box-shadow: 0 0 0 #FFC13B;
                background-color: #FFC13B;
                border-color: #FFC13B;
            }
            .btn-primary:active, .btn-primary:hover, .btn-primary.disabled:active, .btn-primary.disabled:hover {
                background-color: #1E3D59 !important;
            }
        </style>

    @endpush
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Edit Data Ketua UKM</h1>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{route('admin.leader.update',$data->id)}}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">NRP</label>
                                        <div class="col-sm-12 col-md-7">
                                            <select class="selectpicker" name="nrp" data-live-search="true">
                                                <option value="{{$data->leader_nrp}}" data-tokens="{{$data->leader_nrp}}">{{$data->leader_nrp}}</option>

                                            @foreach($mahasiswas as $mhs)
                                                    <option value="{{$mhs->nrp}}" data-tokens="{{$mhs->nrp}}">{{$mhs->nrp}}</option>
                                                @endforeach
                                            </select>
                                            <input type="text" name="nrpOld" value="{{$data->leader_nrp}}" hidden>

                                        </div>
                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Pilih</label>
                                        <div class="col-sm-12 col-md-7">
                                            <select class="selectpicker" name="select" data-live-search="true">
                                                <option value="{{$data->id}}" data-tokens="{{$data->name}}">{{$data->name}}</option>

                                            @foreach($selects as $select)
                                                    <option value="{{$select->id}}" data-tokens="{{$select->name}}">{{$select->name}}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Password</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="password" name="password" id="ukm" class="form-control @error('title') is-invalid @enderror">
                                            @error('password')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button class="btn btn-primary">Edit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @push('script')
        <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
    @endpush
</x-app-layout>
