<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>SI-UKM</title>
    <x-style/>
    @stack('style')
</head>
<body>
    <x-partials.navbar/>
    <x-partials.sidebar/>
    <div class="wrapper">
{{--        @yield('content')--}}
        {{ $slot }}
    </div>
    <x-partials.footer/>

    <x-script/>
    @stack('script')

</body>
</html>
