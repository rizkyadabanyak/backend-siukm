<x-app-layout >
    @push('style')
        <link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/datatables.min.css">
        <link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
        <style>
            .btn-primary, .btn-primary.disabled {
                box-shadow: 0 0 0 #FFC13B;
                background-color: #FFC13B;
                border-color: #FFC13B;
            }
            .btn-primary:active, .btn-primary:hover, .btn-primary.disabled:active, .btn-primary.disabled:hover {
                background-color: #1E3D59 !important;
            }
            .table-top{
                background-color: #1E3D59 !important;
                color: #FFFFFF;
            }
        </style>
    @endpush
        <div class="main-content">
        <section class="section">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="card card-statistic-2">
                        <div class="card-stats">
                            <form action="{{route('admin.actionFilterUkm')}}" method="post">
                                @csrf
                                <div class="row">

                                    <div class="col-12 col-lg-4">
                                        <div class="card-stats-title">Daftar UKM

                                            <select class="form-control" name="id" id="exampleFormControlSelect1">
                                                @foreach($ukms as $data)
                                                    <option value="{{$data->id}}">{{$data->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-12 col-lg-4">
                                        <div class="card-stats-title">Tahun
                                            <select class="form-control" name="year" id="exampleFormControlSelect1">
                                                @foreach($viewUkmsYears as $data)
                                                    <option value="{{$data->year}}">{{$data->year}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4">
                                        <div class="d-flex justify-content-center" style="margin-top: 35px">
                                            <button type="submit" class="btn btn-success ">Simpan</button>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                        <div class="card-chart">
                            <canvas id="sales-chart" height="65"></canvas>
                        </div>
                        <div class="card-icon shadow-primary bg-primary">
                            <i class="fas fa-eye"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>Pengunjung UKM {{$selctUkm->name}}</h4>
                            </div>
                            <div class="card-body">
                                {{$avrgViewUkm}}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="card card-statistic-2">
                        <div class="card-stats">
                            <div class="card-stats-title">Filter Pengunjung Website SI-UKM -
                                <div class="dropdown d-inline">
                                    <a class="font-weight-600 dropdown-toggle" data-toggle="dropdown" href="#" id="orders-month">Pilih</a>
                                    <ul class="dropdown-menu dropdown-menu-sm">
                                        <li class="dropdown-title">Pilih Tahun</li>
                                        @foreach($viewWebYears as $data)
                                            <li><a href="#" class="dropdown-item">{{$data->year}}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card-chart">
                            <canvas id="balance-chart" height="65"></canvas>
                        </div>
                        <div class="card-icon shadow-primary bg-primary">
                                <i class="fas fa-eye"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>Pengunjung Web</h4>
                            </div>
                            <div class="card-body">
                                {{$avrgViewWeb}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <x-partials.notice/>

            <div class="section-body">
                <h2 class="section-title">Statistik Pendaftaran</h2>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                            </div>
                            <div class="card-body">
                                @if(Auth::user()->role_id == '2' || Auth::user()->role_id == '1')
                                    <canvas id="myChart4" width="20"></canvas>
                                @endif
                                <div class="dropdown d-inline mr-2">
                                    @if(Auth::user()->role_id == '2' || Auth::user()->role_id == '1')
                                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Pilih UKM
                                        </button>
                                    @endif
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="{{route('admin.pendaftaran.index')}}">none</a>
                                        @foreach($ukms as $ukm)
                                            <a class="dropdown-item" href="{{route('admin.filter.index',$ukm->id)}}">{{$ukm->name}}</a>
                                        @endforeach
                                    </div>
                                    <br>
                                </div>
                                <br>

                                <div class="table-responsive">
                                    <table class="table table-striped" id="item">
                                        <thead>
                                        <tr class="table-top">
                                            <th style="color: #ffffff">NRP</th>
                                            <th style="color: #ffffff" >Nama Lengkap</th>
                                            <th style="color: #ffffff">Tgl Submit</th>
                                            <th style="color: #ffffff">UKM</th>
                                            <th style="color: #ffffff">Aksi</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @push('script')
        <script src="https://demo.getstisla.com/assets/modules/chart.min.js"></script>


        <script type="text/javascript">

            $(document).ready(function(){
                $('#item').DataTable({
                    @include('components.search')
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{route('admin.pendaftaran.index')}}",
                    },
                    columns: [
                        {
                            data: 'nrp',
                            name: 'nrp'
                        },
                        {
                            data: 'name',
                            name: 'name'
                        },
                        {
                            data: 'created_at',
                            name: 'created_at'
                        },
                        {
                            data: 'ukm_id',
                            name: 'ukm_id'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false
                        },
                    ]
                });
            });

            var ctx = document.getElementById("myChart4").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    datasets: [{
                        data: [
                            @foreach($total as $a)
                                {{$a}}
                            @endforeach
                        ],
                        backgroundColor: [
                            '#191d21',
                            '#63ed7a',
                            '#ffa426',
                            '#fc544b',
                            '#6777ef',
                            '#0e112d',
                            '#9f175e',
                            '#22253e',
                            '#19cd2f',
                            '#0cdd91',
                            '#f1f1f1',
                            '#c7cef1',
                            '#e00c0c',
                            '#000000',
                            '#9aa3dd',
                            '#a2c367',
                            '#124a53',
                            '#6777ef',
                            '#05081d',
                            '#838ce7',
                            '#2e1734',
                            '#2d2e3e',
                            '#e095a3',
                            '#091d51',
                            '#7138e2',
                            '#342929',
                            '#0d1122',
                            '#afb9de',
                            '#f671a9',

                        ],
                        label: 'Dataset 1'
                    }],
                    labels: [
                        @foreach($nama as $a)
                        '{{$a}}',
                        @endforeach
                    ],
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'bottom',
                    },
                }
            });

            var balance_chart = document.getElementById("balance-chart").getContext('2d');

            var balance_chart_bg_color = balance_chart.createLinearGradient(0, 0, 0, 70);
            balance_chart_bg_color.addColorStop(0, 'rgba(63,82,227,.2)');
            balance_chart_bg_color.addColorStop(1, 'rgba(63,82,227,0)');

            var myChart = new Chart(balance_chart, {
                type: 'line',
                data: {
                    labels: [
                        @foreach($viewWebs as $data)
                        '{{$data->month}}-{{$data->year}}',
                        @endforeach
                    ],
                    datasets: [{
                        label: 'Pengunjung',
                        data: [
                            @foreach($viewWebs as $data)
                            {{$data->view}},
                            @endforeach
                        ],
                        backgroundColor: balance_chart_bg_color,
                        borderWidth: 3,
                        borderColor: 'rgba(63,82,227,1)',
                        pointBorderWidth: 0,
                        pointBorderColor: 'transparent',
                        pointRadius: 3,
                        pointBackgroundColor: 'transparent',
                        pointHoverBackgroundColor: 'rgba(63,82,227,1)',
                    }]
                },
                options: {
                    layout: {
                        padding: {
                            bottom: -1,
                            left: -1
                        }
                    },
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                            gridLines: {
                                display: false,
                                drawBorder: false,
                            },
                            ticks: {
                                beginAtZero: true,
                                display: false
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                drawBorder: false,
                                display: false,
                            },
                            ticks: {
                                display: false
                            }
                        }]
                    },
                }
            });

            var sales_chart = document.getElementById("sales-chart").getContext('2d');

            var sales_chart_bg_color = sales_chart.createLinearGradient(0, 0, 0, 80);
            balance_chart_bg_color.addColorStop(0, 'rgba(63,82,227,.2)');
            balance_chart_bg_color.addColorStop(1, 'rgba(63,82,227,0)');

            var myChart = new Chart(sales_chart, {
                type: 'line',
                data: {
                    labels: [
                        @foreach($viewUkms as $data)
                            '{{$data->month}}-{{$data->year}}',
                        @endforeach
                    ],
                    datasets: [{
                        label: 'view',
                        data: [
                            @foreach($viewUkms as $data)
                            {{$data->view}},
                            @endforeach
                            ],
                        borderWidth: 2,
                        backgroundColor: balance_chart_bg_color,
                        borderWidth: 3,
                        borderColor: 'rgba(63,82,227,1)',
                        pointBorderWidth: 0,
                        pointBorderColor: 'transparent',
                        pointRadius: 3,
                        pointBackgroundColor: 'transparent',
                        pointHoverBackgroundColor: 'rgba(63,82,227,1)',
                    }]
                },
                options: {
                    layout: {
                        padding: {
                            bottom: -1,
                            left: -1
                        }
                    },
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                            gridLines: {
                                display: false,
                                drawBorder: false,
                            },
                            ticks: {
                                beginAtZero: true,
                                display: false
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                drawBorder: false,
                                display: false,
                            },
                            ticks: {
                                display: false
                            }
                        }]
                    },
                }
            });
        </script>
    @endpush
</x-app-layout>
