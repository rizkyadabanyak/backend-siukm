<x-app-layout >
    @push('style')
        <link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/datatables.min.css">
        <link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
        <style>
            .btn-primary, .btn-primary.disabled {
                box-shadow: 0 0 0 #FFC13B;
                background-color: #FFC13B;
                border-color: #FFC13B;
            }
            .btn-primary:active, .btn-primary:hover, .btn-primary.disabled:active, .btn-primary.disabled:hover {
                background-color: #1E3D59 !important;
            }
            .table-top{
                background-color: #1E3D59 !important;
                color: #FFFFFF;
            }
        </style>
    @endpush
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Statistika</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Modules</a></div>
                    <div class="breadcrumb-item">DataTables</div>
                </div>
            </div>
            <x-partials.notice/>

            <div class="section-body">
                <h2 class="section-title">Statistik Pendaftaran</h2>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                            </div>
                            <div class="card-body">
                                <canvas id="myChart4" width="20"></canvas>
                                <div class="dropdown d-inline mr-2">
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Pilih Tahun
                                    </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="{{route('admin.pendaftaran.index')}}">none</a>
                                        @foreach($ukms as $ukm)
                                            <a class="dropdown-item" href="{{route('admin.filter.index',$ukm->id)}}">{{$ukm->name}}</a>
                                        @endforeach
                                    </div>
                                    <br>
                                </div>
                                <br>

                                <div class="table-responsive">
                                    <table class="table table-striped" id="item">
                                        <thead>
                                        <tr class="table-top">
                                            <th style="color: #ffffff">NRP</th>
                                            <th style="color: #ffffff" >Nama Lengkap</th>
                                            <th style="color: #ffffff">Tgl Submit</th>
                                            <th style="color: #ffffff">UKM</th>
                                            <th style="color: #ffffff">Aksi</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @push('script')
        <script src="https://demo.getstisla.com/assets/modules/chart.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function(){
                $('#item').DataTable({
                    @include('components.search'),
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{route('admin.filter.index',$id)}}",
                    },
                    columns: [
                        {
                            data: 'nrp',
                            name: 'nrp'
                        },
                        {
                            data: 'name',
                            name: 'name'
                        },
                        {
                            data: 'created_at',
                            name: 'created_at'
                        },
                        {
                            data: 'ukm_id',
                            name: 'ukm_id'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false
                        },
                    ]
                });
            });

            var ctx = document.getElementById("myChart4").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    datasets: [{
                        data: [
                            @foreach($total as $a)
                            {{$a}}
                            @endforeach
                        ],
                        backgroundColor: [
                            '#191d21',
                            '#63ed7a',
                            '#ffa426',
                            '#fc544b',
                            '#6777ef',
                            '#0e112d',
                            '#9f175e',
                            '#22253e',
                            '#19cd2f',
                            '#0cdd91',
                            '#f1f1f1',
                            '#c7cef1',
                            '#e00c0c',
                            '#000000',
                            '#9aa3dd',
                            '#a2c367',
                            '#124a53',
                            '#6777ef',
                            '#05081d',
                            '#838ce7',
                            '#2e1734',
                            '#2d2e3e',
                            '#e095a3',
                            '#091d51',
                            '#7138e2',

                        ],
                        label: 'Dataset 1'
                    }],
                    labels: [
                        @foreach($nama as $a)
                            '{{$a}}',
                        @endforeach
                    ],
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'bottom',
                    },
                }
            });
        </script>
    @endpush
</x-app-layout>
