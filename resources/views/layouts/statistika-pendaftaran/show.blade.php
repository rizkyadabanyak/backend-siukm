<x-app-layout >
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Data Mahasiswa</h1>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row"> 
                                    <div class="col-2">NRP</div>
                                    <div class="col-10">: {{$data->nrp}}</div>
                                </div>
                                <div class="row"> 
                                    <div class="col-2">Nama</div>
                                    <div class="col-10">: {{$data->mahasiswa->name}}</div>
                                </div>
                                <div class="row"> 
                                    <div class="col-2">Jurusan</div>
                                    <div class="col-10">: {{$data->mahasiswa->major}}</div>
                                </div>
                                <div class="row"> 
                                    <div class="col-2">Alamat</div>
                                    <div class="col-10">: {{$data->mahasiswa->address}}</div>
                                </div>
                                <div class="row"> 
                                    <div class="col-2">No Telepon</div>
                                    <div class="col-10">: {{$data->mahasiswa->phone_number}}</div>
                                </div>
                                <div class="row"> 
                                    <div class="col-2">UKM yang dituju</div>
                                    <div class="col-10">: {{$data->ukms->name}}</div>
                                </div>
                                <div class="row"> 
                                    <div class="col-2">Alasan</div>
                                    <div class="col-10">: {{$data->reason}}</div>
                                </div>
                                <div class="row"> 
                                    <div class="col-2">Tanggal submit</div>
                                    <div class="col-10">: {{date('d-m-Y', strtotime($data->created_at))}}</div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</x-app-layout>
