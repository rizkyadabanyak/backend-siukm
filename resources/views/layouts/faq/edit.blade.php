<x-app-layout >
    <style>
        .btn-primary, .btn-primary.disabled {
            box-shadow: 0 0 0 #FFC13B;
            background-color: #FFC13B;
            border-color: #FFC13B;
        }
        .btn-primary:active, .btn-primary:hover, .btn-primary.disabled:active, .btn-primary.disabled:hover {
            background-color: #1E3D59 !important;
        }
    </style>

    @push('style')
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    @endpush
<div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Daftar FAQ</h1>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="container">
                                <div class="d-flex justify-content-between pt-5">
                                    <div>
                                        <h4>Edit FAQ UKM</h4>

                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <form action="{{route('admin.faqs.update',$data->id)}}" method="post">
                                    @csrf
                                    @method('put')
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">UKM</label>
                                        <div class="col-sm-12 col-md-7">
                                            <select class="selectpicker" data-width="fit" data-size="5"  name="ukm" data-live-search="true">
                                                <option value=" {{$data->ukms->id}}" data-tokens="{{$data->ukms->name}}" >{{$data->ukms->name}}</option>
                                                @foreach($ukms as $ukm)
                                                    <option value="{{$ukm->id}}" data-tokens="{{$ukm->name}}" >{{$ukm->name}}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Pertanyaan</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="value" value=" {{$data->value}}" id="value" class="form-control @error('title') is-invalid @enderror">
                                            @error('title')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jawaban</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="answer" value=" {{$data->answer}}" id="answer" class="form-control @error('title') is-invalid @enderror">
                                            @error('title')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button class="btn btn-primary">Edit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</x-app-layout>
