<x-app-layout >
    <style>
        .btn-primary, .btn-primary.disabled {
            box-shadow: 0 0 0 #FFC13B;
            background-color: #FFC13B;
            border-color: #FFC13B;
        }
        .btn-primary:active, .btn-primary:hover, .btn-primary.disabled:active, .btn-primary.disabled:hover {
            background-color: #1E3D59 !important;
        }
        .table-top{
            background-color: #1E3D59 !important;
            color: #FFFFFF;
        }
    </style>

    @push('style')
        <link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/datatables.min.css">
        <link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css">
    @endpush
        <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Daftar FAQ</h1>
            </div>
            <x-partials.notice/>

            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4><a href="{{route('admin.faqs.create')}}" class="btn btn-primary">TAMBAH DATA</a> </h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-width" id="item">
                                        <thead>
                                            <tr class="table-top">
                                                <th style="color: #ffffff">UKM</th>
                                                <th style="color: #ffffff">Pertanyaan</th>
                                                <th style="color: #ffffff">Jawaban</th>
                                                <th style="color: #ffffff">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
        @push('script')
            <script type="text/javascript">
                $(document).ready(function(){
                    $('#item').DataTable({
                        @include('components.search')
                        processing: true,
                        serverSide: true,
                        ajax: {
                            url: "{{route('admin.faqs.index')}}",
                        },
                        columns: [
                            {
                                data: 'ukm',
                                name: 'ukm'
                            },
                            {
                                data: 'value',
                                name: 'value'
                            },
                            {
                                data: 'answer',
                                name: 'answer'
                            },
                            {
                                data: 'action',
                                name: 'action',
                                orderable: false
                            },

                        ]
                    });
                });
            </script>

        @endpush
</x-app-layout>

