<x-app-layout >
    <style>
    
    </style>
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Detail UKM</h1>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row"> 
                                    <div class="col-1">Nama</div>
                                    <div class="col-11">: {!! $data->name !!}</div>
                                </div>
                                <div class="row"> 
                                    <div class="col-1">Kategori</div>
                                    <div class="col-11">: {!! $data->categories->name!!}</div>
                                </div>
                                <div class="row"> 
                                    <div class="col-1">Deskripsi</div>
                                    <div class="col-11">: {!! $data->desc!!}</div>
                                </div>
                                <div class="row"> 
                                    <div class="col-1">Visi</div>
                                    <div class="col-11">: {!! $data->vision!!}</div>
                                </div>
                                <div class="row"> 
                                    <div class="col-1">Misi</div>
                                    <div class="col-11">: {!! $data->mission!!}</div>
                                </div>
                                <div class="row"> 
                                    <div class="col-1">Benefit</div>
                                    <div class="col-11">: {!! $data->benefit!!}</div>
                                </div>
                                <div class="row"> 
                                    <div class="col-1">Jadwal</div>
                                    <div class="col-11">: {!! $data->schedule!!}</div>
                                </div>
                                <div class="row"> 
                                    <div class="col-1">Kata kunci</div>
                                    <div class="col-11">: @foreach($data->tags as $tag)
                                        {!! $tag->name!!} @endforeach
                                    </div>
                                </div>
                                <div class="row"> 
                                    <div class="col-1">Tanggal mulai</div>
                                    <div class="col-11">: {!! $data->start_date!!}</div>
                                </div>
                                <div class="row"> 
                                    <div class="col-1">Tanggal berakhir</div>
                                    <div class="col-11">: {!! $data->end!!}</div>
                                </div>
                                <div class="row"> 
                                    <div class="col-1">Staff:</div>
                                    <div class="col-11">: <br>
                                    @foreach($data->staffs as $staff)
                                        Nama : {{$staff->mahasiswa->name}}<br> Posisi : {{$staff->position}} <br><br>
                                    @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

</x-app-layout>
