<x-app-layout >
    @push('style')
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    @endpush
<div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Detail UKM</h1>
            </div>
{{--            @include('admin.layouts.partials.notice')--}}

            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="container">
                                <div class="d-flex justify-content-between pt-5">
                                    <div>
                                        <h4>Struktur UKM</h4>
                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <form action="{{route('admin.staff.store',$id)}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 ">Periode </label>
                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Start Date</label>
                                        <div class="col-sm-6 col-md-7">
                                            <input type="date" name="start_date" class="form-control @error('title') is-invalid @enderror">
                                            @error('start_date')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">End Date</label>
                                        <div class="col-sm-6 col-md-7">
                                            <input type="date" name="end_date" class="form-control @error('title') is-invalid @enderror">
                                            @error('end_date')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <br><br>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Data Staff</label>
                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">NRP</label>
                                        <div class="col-sm-12 col-md-7">
                                            <select class="selectpicker" data-width="fit" data-size="5" name="nrp[]" data-live-search="true">
                                                @foreach($users as $user)
                                                    <option value="{{$user->nrp}}" data-tokens="{{$user->nrp}}">{{$user->nrp}}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Posisi</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="position[]" class="form-control @error('title') is-invalid @enderror">
                                            @error('position')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="staff">

                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <a style="color: white" class="btn btn-success float-right addstaff">New Staff</a>
                                        </div>
                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            @if($data->final == '0')
                                                <a href="{{route('admin.ukm.create')}}" style="color: white" class="btn btn-primary">Kembali</a>
                                            @endif
                                            <button class="btn btn-primary">Selesai</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
        @push('script')
            <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

            <script>
                $('.addstaff').on('click',function (){
                    addstaff();
                });
                function addstaff(){
                    {{--var staff = '<div class="form-group row mb-4"> <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Data Staff</label> </div><div class="form-group row mb-4"> <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Position</label> <div class="col-sm-12 col-md-7"> <input type="text" name="role[]" id="role" class="form-control @error('title') is-invalid @enderror"> @error('name') <div class="alert alert-danger">{{ $message }}</div> @enderror </div> </div> <div class="form-group row mb-4"> <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Name</label> <div class="col-sm-12 col-md-7"> <input type="text" name="name[]" id="name" class="form-control @error('title') is-invalid @enderror"> @error('name') <div class="alert alert-danger">{{ $message }}</div> @enderror </div> </div><div class="form-group row mb-4"> <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label> <div class="col-sm-12 col-md-7"> <a style="color: white" class="btn btn-danger float-right remove">Remove</a></div></div>';--}}
                        var staff = '<div class="form-group row mb-4"> <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Data Staff</label> </div><div class="form-group row mb-4"> <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">NRP</label> <div class="col-sm-12 col-md-7"> <select class="form-control select2" name="nrp[]" data-live-search="true"> @foreach($users as $user) <option value="{{$user->nrp}}" data-tokens="{{$user->nrp}}">{{$user->nrp}}</option> @endforeach </select> </div> </div> <div class="form-group row mb-4"> <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Position</label> <div class="col-sm-12 col-md-7"> <input type="text" name="position[]" class="form-control @error('title') is-invalid @enderror"> @error('position') <div class="alert alert-danger">{{ $message }}</div> @enderror </div> </div>'
                    $('.staff').append(staff);
                };
                $('.remove').live('click', function(){
                    $(this).parent().parent().parent().remove();
                });
            </script>
        @endpush


</x-app-layout>

