<x-app-layout >
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Galeri UKM</h1>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="card">
                            <div class="container">
                                <div class="d-flex justify-content-between pt-5">
                                    <div>
                                        <h4>Tambah Gambar</h4>

                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <form action="{{route('admin.galleries.create',$data->id)}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Gambar</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="file" name="image[]" id="image" multiple="true">
                                        </div>

                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button class="btn btn-primary">Simpan</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card">
                            <div class="container">
                                <div class="d-flex justify-content-between pt-5">
                                    <div>
                                        <h4>Galleries</h4>

                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Gambar Thumbnail</label>
                                    <div class="col-sm-12 col-md-7">

                                        <div class="row gutters-sm">
                                            @forelse($images as $img)
                                                <div class="col-6 col-sm-4">
                                                    <label class="imagecheck mb-4">
                                                        <input name="images" id="thumbnail" type="checkbox" value="{{$img->id}}" class="imagecheck-input checkBoxClass" />
                                                        <figure class="imagecheck-figure">
                                                            <img src="{{asset($img->name)}}" alt="}" class="imagecheck-image">
                                                        </figure>
                                                    </label>
                                                </div>
                                            @empty
                                                <a>Kamu tidak memiliki galeri</a>
                                            @endforelse
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="checkbox" id="checkAll">Check all</input>

                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                    <div class="col-sm-12 col-md-7">
                                        <a href="#" class="btn btn-danger" id="deleteRecord">Hapus</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="container">
                                <div class="d-flex justify-content-between pt-5">
                                    <div>
                                        <h4>Prestasi</h4>

                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped" id="item">
                                            <thead>
                                            <tr>
                                                <th>Gambar</th>
                                                <th>Nama</th>
                                                <th>Posisi</th>
                                                <th>Deskripsi</th>
                                                <th>Aksi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($achievements as $achievement)
                                                <td>
                                                    <img src="{{asset($achievement->galleries->name)}}" class="img-fluid img-thumbnail">
                                                </td>
                                                <td>
                                                    {{$achievement->name}}
                                                </td>
                                                <td>
                                                {{$achievement->position}}
                                                <td>
                                                    {!! $achievement->desc !!}
                                                </td>
                                                <td>
                                                    <a href="{{route('admin.destroy.achievements',[$achievement->ukm_id,$achievement->gallery_id])}}" class="btn btn-danger"><i class="fa fa-pencil"></i> delete</a>                                                </td>
                                            @endforeach

                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="col-12 col-md-6">
                        <div class="card">
                            <div class="container">
                                <div class="d-flex justify-content-between pt-5">
                                    <div>
                                        <h4>Thumbnail</h4>

                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <form action="{{route('admin.galleries.thumbnail')}}" method="post">
                                    @csrf
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Gambar thumbnail</label>
                                        <div class="col-sm-12 col-md-7">

                                            <div class="row gutters-sm">
                                                @forelse($images as $img)
                                                    <div class="col-6 col-sm-4">
                                                        <label class="imagecheck mb-4">
                                                            @if($img->thumbnail == "1")
                                                                <input type="text" name="tmp" value="{{$img->id}}" hidden>
                                                            @endif
                                                            <input name="thumbnail" id="thumbnail" type="radio" value="{{$img->id}}" class="imagecheck-input" {{ $img->thumbnail == "1" ? 'checked' : '' }} />
                                                            <figure class="imagecheck-figure">
                                                                <img src="{{asset($img->name)}}" alt="}" class="imagecheck-image">
                                                            </figure>
                                                        </label>
                                                    </div>
                                                @empty
                                                    <a>Kamu tidak memiliki galeri</a>
                                                @endforelse
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button class="btn btn-primary">Tambah</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card">
                            <div class="container">
                                <div class="d-flex justify-content-between pt-5">
                                    <div>
                                        <h4>Prestasi</h4>

                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <form action="{{route('admin.galleries.achievements',$data->id)}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row gutters-sm">
                                        @forelse($images as $img)
                                            <div class="col-6 col-sm-4">
                                                <label class="imagecheck mb-4">
                                                    <input name="thumbnail" id="thumbnail" type="radio" value="{{$img->id}}" class="imagecheck-input" {{ $img->thumbnail == "1" ? 'checked' : '' }} />
                                                    <figure class="imagecheck-figure">
                                                        <img src="{{asset($img->name)}}" alt="}" class="imagecheck-image">
                                                    </figure>
                                                </label>
                                            </div>
                                        @empty
                                            <a>Kamu tidak memiliki galeri</a>
                                        @endforelse
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="name" id="name" class="form-control @error('title') is-invalid @enderror">
                                            @error('name')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Posisi</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="position" id="position" class="form-control @error('title') is-invalid @enderror">
                                            @error('position')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Deskripsi</label>
                                        <div class="col-sm-12 col-md-7">
                                            <!-- <textarea class="summernote-simple {{ $errors->has('desc') ? 'is-invalid' :'' }}"
                                                      name="desc" required></textarea>
                                            @error('desc')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror -->

                                            <input type="text" name="desc" id="desc" class="form-control @error('title') is-invalid @enderror">
                                            @error('desc')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button class="btn btn-primary">Simpan</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>




                </div>
            </div>
        </section>
    </div>
    <script>
        $(function (e){
            $("#checkAll").click(function (){
                $(".checkBoxClass").prop('checked',$(this).prop('checked'));
            });
            $("#deleteRecord").click(function (e){
                e.preventDefault();
                var images = [];

                $("input:checkbox[name=images]:checked").each(function (){
                    images.push($(this).val());
                });
                $.ajax({
                    url : "{{route('admin.galleries.selectDelete')}}",
                    type:"DELETE",
                    data:{
                        _token:$("input[name=_token]").val(),
                        images:  images
                    },
                    success:function (response){
                        $.each(images,function (key,val){
                            $("#images"+val).remove();
                        })
                    }
                });
                window.location.href=window.location.href;
            });
        });
    </script>
</x-app-layout>
