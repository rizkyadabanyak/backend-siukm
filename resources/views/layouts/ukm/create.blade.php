<x-app-layout >
    <style>
        .btn-primary, .btn-primary.disabled {
            box-shadow: 0 0 0 #FFC13B;
            background-color: #FFC13B;
            border-color: #FFC13B;
        }
        .btn-primary:active, .btn-primary:hover, .btn-primary.disabled:active, .btn-primary.disabled:hover {
            background-color: #1E3D59 !important;
        }
    </style>

    @push('style')
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

    @endpush
        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <h1>Daftar UKM</h1>
                </div>

                <div class="section-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <form action="{{route('admin.ukm.store')}}" method="post" enctype="multipart/form-data">
                                        @csrf
{{--                                        <div class="form-group row mb-4">--}}
{{--                                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Ketua</label>--}}
{{--                                            <div class="col-sm-12 col-md-7">--}}
{{--                                                <select class="selectpicker" name="leader" data-live-search="true">--}}

{{--                                                @foreach($users as $user)--}}
{{--                                                        <option value="{{$user->nrp}}" data-tokens="{{$user->nrp}}">{{$user->nrp}}</option>--}}
{{--                                                    @endforeach--}}
{{--                                                </select>--}}

{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="form-group row mb-4">
                                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama</label>
                                            <div class="col-sm-12 col-md-7">
                                                <input type="text" name="name" id="name" class="form-control @error('title') is-invalid @enderror" value="{{($data != null) ? $data->name : '' }}">
                                                @error('name')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row mb-4">
                                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kategori</label>
                                            <div class="col-sm-12 col-md-7">
                                                <select class="form-control select2" name="category_id">
                                                    @if($data != null)
                                                        @if($data->category_id != null)
                                                            <option value="{{$data->category_id}}">{{$data->categories->name}}</option>
                                                        @endif
                                                        @foreach($categories as $category)
                                                            @if($category->id != $data->category_id)
                                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        @foreach($categories as $category)
                                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                @error('category_id')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row mb-4">
                                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kata Kunci</label>
                                            <div class="col-sm-12 col-md-7">
                                                <select class="selectpicker form-control select2" data-width="fit" data-size="5" name="tag[]" multiple data-live-search="true" >
                                                    @if($data != null)
                                                        @foreach($tags as $tag)
                                                            <option value="{{$tag->id}}"
                                                                    @foreach($data->tags as $myTag)
                                                                    @if(in_array($tag->id,old('tag',[$myTag->id]))) selected="selected"
                                                                @endif
                                                                @endforeach
                                                            >{{$tag->name}}</option>
                                                        @endforeach
                                                    @else
                                                        @foreach($tags as $tag)
                                                            <option value="{{$tag->id}}" @if(in_array($tag->id,old('tag',[]))) selected="selected" @endif >{{$tag->name}}</option>
                                                        @endforeach
                                                    @endif

                                                </select>
                                                @error('tag')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row mb-4">
                                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Logo</label>
                                            <div class="col-sm-12 col-md-7">
                                                <input type="file" name="image" id="image" value="{{($data != null) ? $data->logo : '' }}">
                                                <input name="newImage" value="{{($data != null) ? $data->logo : '' }}" hidden>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-4">
                                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jadwal</label>
                                            <div class="col-sm-12 col-md-7">
                                            <!-- <textarea class="summernote-simple {{ $errors->has('schedule') ? 'is-invalid' :'' }}"
                                                      name="schedule" required> {{($data != null) ? $data->schedule : old('schedule') }} </textarea>
                                                @error('schedule')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror -->

                                                <input type="text" name="schedule" id="schedule" class="form-control @error('title') is-invalid @enderror" value="{{($data != null) ? $data->schedule : '' }}">
                                                @error('schedule')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>


                                        </div>
                                        <div class="form-group row mb-4">
                                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Manfaat</label>
                                            <div class="col-sm-12 col-md-7">
                                            <!-- <textarea class="summernote-simple {{ $errors->has('benefit') ? 'is-invalid' :'' }}"
                                                      name="benefit" required>{{($data != null) ? $data->benefit : old('benefit') }}</textarea>
                                                @error('benefit')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror -->


                                                <input type="text" name="benefit" id="benefit" class="form-control @error('title') is-invalid @enderror" value="{{($data != null) ? $data->benefit : '' }}">
                                                @error('benefit')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror


                                            </div>
                                        </div>
                                        <div class="form-group row mb-4">
                                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Visi</label>
                                            <div class="col-sm-12 col-md-7">
                                            <!-- <textarea class="summernote-simple {{ $errors->has('vision') ? 'is-invalid' :'' }}"
                                                      name="vision" required>{{($data != null) ? $data->vision : old('vision') }}</textarea>
                                                @error('vision')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror -->

                                                <input type="text" name="vision" id="vision" class="form-control @error('title') is-invalid @enderror" value="{{($data != null) ? $data->vision : '' }}">
                                                @error('vision')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror


                                            </div>
                                        </div>
                                        <div class="form-group row mb-4">
                                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Misi</label>
                                            <div class="col-sm-12 col-md-7">
                                            <!-- <textarea class="summernote-simple {{ $errors->has('mission') ? 'is-invalid' :'' }}"
                                                      name="mission" required>{{($data != null) ? $data->mission : old('mission') }}</textarea>
                                                @error('dsc')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror -->

                                                <input type="text" name="mission" id="mission" class="form-control @error('title') is-invalid @enderror" value="{{($data != null) ? $data->mission : '' }}">
                                                @error('mission')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror

                                            </div>
                                        </div>
                                        <div class="form-group row mb-4">
                                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Whatsapp</label>
                                            <div class="col-sm-12 col-md-7">
                                            <!-- <textarea class="summernote-simple {{ $errors->has('phone_number') ? 'is-invalid' :'' }}"
                                                      name="phone_number" required>{{($data != null) ? $data->phone_number : old('phone_number') }}</textarea>
                                                @error('phone_number')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror -->

                                                <input type="text" name="phone_number" id="phone_number" class="form-control @error('title') is-invalid @enderror" value="{{($data != null) ? $data->phone_number : '' }}">
                                                @error('phone_number')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror

                                            </div>
                                        </div>
                                        <div class="form-group row mb-4">
                                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Instagram</label>
                                            <div class="col-sm-12 col-md-7">
                                            <!-- <textarea class="summernote-simple {{ $errors->has('ig') ? 'is-invalid' :'' }}"
                                                      name="ig" required>{{($data != null) ? $data->phone_number : old('ig') }}</textarea>
                                                @error('ig')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror -->

                                                <input type="text" name="ig" id="ig" class="form-control @error('title') is-invalid @enderror" value="{{($data != null) ? $data->ig : '' }}">
                                                @error('ig')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror


                                            </div>
                                        </div>
                                        <div class="form-group row mb-4">
                                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Line</label>
                                            <div class="col-sm-12 col-md-7">
                                            <!-- <textarea class="summernote-simple {{ $errors->has('line') ? 'is-invalid' :'' }}"
                                                      name="line" required>{{($data != null) ? $data->phone_number : old('line') }}</textarea>
                                                @error('line')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror -->

                                                <input type="text" name="line" id="line" class="form-control @error('title') is-invalid @enderror" value="{{($data != null) ? $data->line : '' }}">
                                                @error('line')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror



                                            </div>
                                        </div>
                                        <div class="form-group row mb-4">
                                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Deskripsi</label>
                                            <div class="col-sm-12 col-md-7">
                                            <!-- <textarea class="summernote-simple {{ $errors->has('dsc') ? 'is-invalid' :'' }}"
                                                      name="dsc" required>{{($data != null) ? $data->desc : old('dsc') }}</textarea>
                                                @error('dsc')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror -->

                                                <input type="text" name="dsc" id="dsc" class="form-control @error('title') is-invalid @enderror" value="{{($data != null) ? $data->dsc : '' }}">
                                                @error('dsc')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror

                                            </div>
                                        </div>
                                        <div class="form-group row mb-4">
                                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                            <div class="col-sm-12 col-md-7">
                                                <button class="btn btn-primary">Selanjutnya</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    @push('script')
            <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

            <script>
                $(document).ready(function() {
                    $('#summernote').summernote({
                        dialogsInBody: true,
                        minHeight: 150,
                        toolbar: [
                            ['style', ['bold', 'italic', 'underline', 'clear']],
                            ['font', ['strikethrough']],
                            ['para', ['paragraph']]
                        ]
                    });
                });


            </script>
    @endpush
</x-app-layout>
