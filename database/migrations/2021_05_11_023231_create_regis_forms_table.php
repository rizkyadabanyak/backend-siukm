<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegisFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regis_forms', function (Blueprint $table) {
            $table->id();
            $table->foreignId('ukm_id')->constrained('ukms');
            $table->bigInteger('nrp');
            $table->text('reason');
            $table->timestamps();

            $table->foreign('nrp')->references('nrp')->on('mahasiswas')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regis_forms');
    }
}
