<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUkmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ukms', function (Blueprint $table) {
            $table->id();
            $table->foreignId('category_id')->nullable()->constrained('categories');
            $table->bigInteger('leader_nrp')->unique()->nullable();
            $table->text('name')->nullable();
            $table->text('slug')->nullable();
            $table->text('benefit')->nullable();
            $table->text('mission')->nullable();
            $table->text('vision')->nullable();
            $table->text('schedule')->nullable();
            $table->text('logo')->nullable();
            $table->text('phone_number')->nullable();
            $table->text('ig')->nullable();
            $table->text('line')->nullable();
            $table->enum('final',[0,1])->nullable()->default(0);
            $table->text('desc')->nullable();
            $table->enum('status',['active','non-active'])->default('active');
            $table->string('password')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->foreign('leader_nrp')->references('nrp')->on('mahasiswas')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ukms');
    }
}
