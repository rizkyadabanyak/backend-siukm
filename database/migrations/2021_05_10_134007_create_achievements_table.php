<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAchievementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('achievements', function (Blueprint $table) {
            $table->foreignId('ukm_id')->constrained('ukms');
            $table->foreignId('gallery_id')->constrained('galleries');
            $table->string('name');
            $table->string('position');
            $table->text('desc');

            $table->timestamps();

            $table->primary(['ukm_id','gallery_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('achievements');
    }
}
