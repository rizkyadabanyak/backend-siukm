<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [
                'name' => "super-admin",
            ],
            [
                'name' => "lmb",
            ],
            [
                'name' => "ukm",
            ],
            [
                'name' => "mhs",
            ],

        ];

        \DB::table('roles')->insert($datas);
    }
}
