<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [
                'nrp' => 2103191001,
                'role_id' => 1,
                'name' => 'Rizky Putra Ramadan',
                'phone_number' => '0895627543357',
                'major' => 'Teknik Informatika',
                'address' => 'perum taman sidorejo n-18',
                'email' => 'riskipatra5@siukm.com',
                'lead_ukm' => '1'


            ],
            [
                'nrp' => 2103191002,
                'role_id' => 2,
                'name' => 'ini lmb',
                'phone_number' => '1111111111',
                'major' => 'Teknik Informatika lmb',
                'address' => 'ini alamat lmb',
                'email' => 'lmb@siukm.com',
                'lead_ukm' => '1'

            ],
            [
                'nrp' => 2103191003,
                'role_id' => 3,
                'name' => 'ini ketua ukm',
                'phone_number' => '222222222',
                'major' => 'Teknik Informatika ketua ukm',
                'address' => 'ini alamat ketua lmb',
                'email' => 'ukm@siukm.com',
                'lead_ukm' => '1'

            ],
        ];

        \DB::table('mahasiswas')->insert($datas);

        $datas = [
            [
                'leader_nrp' => 2103191001,
                'password' =>  Hash::make('28112000'),
            ],
            [
                'leader_nrp' => 2103191002,
                'password' =>  Hash::make('adminlmb'),
            ],
            [
                'leader_nrp' => 2103191003,
                'password' =>  Hash::make('adminukm'),
            ],
        ];

        \DB::table('ukms')->insert($datas);

    }
}
