<?php

use App\Models\User;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::get('/',[\App\View\Components\AppLayout::class,'render']);
Route::group(['as'=>'admin.','middleware'=>'auth'], function(){

    Route::get('/', [\App\Http\Controllers\Layouts\RegisController::class,'pendaftaran'])->name('index');
    Route::get('/test', function(){
        return view('test');
    })->name('index');

    Route::resource('ukm',\App\Http\Controllers\Layouts\UkmController::class);

    Route::resource('mahasiswa',\App\Http\Controllers\Layouts\MahasiswaController::class);
    Route::get('mahasiswa/{id}/destroy', [\App\Http\Controllers\Layouts\MahasiswaController::class,'destroy'])->name('mahasiswaDestroy');

    Route::resource('leader',\App\Http\Controllers\Layouts\LeaderController::class);
    Route::get('leader/{id}/destroy', [\App\Http\Controllers\Layouts\LeaderController::class,'destroy'])->name('leaderDestroy');

    Route::get('ukm/{id}/destroy', [\App\Http\Controllers\Layouts\UkmController::class,'destroy'])->name('ukmDestroy');

    Route::get('ukm/galleries/{id}', [\App\Http\Controllers\Layouts\GalleryController::class,'index'])->name('galleries.index');
    Route::post('ukm/galleries/{id}/create', [\App\Http\Controllers\Layouts\GalleryController::class,'create'])->name('galleries.create');
    Route::post('ukm/galleries/thumbnail', [\App\Http\Controllers\Layouts\GalleryController::class,'image'])->name('galleries.thumbnail');
    Route::delete('ukm/galleries/selectDelete', [\App\Http\Controllers\Layouts\GalleryController::class,'selectDelete'])->name('galleries.selectDelete');
    Route::get('ukm/Achievements/{ukm_id}/{gallery_id}', [\App\Http\Controllers\Layouts\GalleryController::class,'destroyAchievements'])->name('destroy.achievements');

    Route::post('ukm/galleries/{id}/create/achievements', [\App\Http\Controllers\Layouts\GalleryController::class,'achievements'])->name('galleries.achievements');

    Route::get('ukm/staff/{id}', [\App\Http\Controllers\Layouts\StaffController::class,'create'])->name('staff.create');
    Route::post('ukm/staff/{id}', [\App\Http\Controllers\Layouts\StaffController::class,'store'])->name('staff.store');
    Route::get('ukm/staff/{idUKM}/delete', [\App\Http\Controllers\Layouts\StaffController::class,'destroy'])->name('staff.destroy');

    Route::resource('tags', \App\Http\Controllers\Layouts\TagController::class);
    Route::get('tags/{id}/destroy', [\App\Http\Controllers\Layouts\TagController::class,'destroy'])->name('tagsDestroy');

    Route::resource('categories', \App\Http\Controllers\Layouts\CategoryController::class);
    Route::get('categories/{id}/destroy', [\App\Http\Controllers\Layouts\CategoryController::class,'destroy'])->name('categoriesDestroy');

    Route::resource('carousels', \App\Http\Controllers\Layouts\CarouselsController::class);
    Route::get('carousels/{id}/destroy', [\App\Http\Controllers\Layouts\CarouselsController::class,'destroy'])->name('carouselsDestroy');

    Route::get('statistika-pendaftaran/', [\App\Http\Controllers\Layouts\RegisController::class,'pendaftaran'])->name('pendaftaran.index');

    Route::get('statistika-pendaftaran/{id}', [\App\Http\Controllers\Layouts\RegisController::class,'filter'])->name('filter.index');
    Route::get('statistika-pendaftaran/view-web/{year}', [\App\Http\Controllers\Layouts\RegisController::class,'filterWeb'])->name('filterWeb.index');
    Route::post('statistika-pendaftaran/view-ukm/', [\App\Http\Controllers\Layouts\RegisController::class,'actionFilterWeb'])->name('actionFilterUkm');
    Route::get('statistika-pendaftaran/view-ukm/{ukm}/{year}', [\App\Http\Controllers\Layouts\RegisController::class,'Filterukm'])->name('filterUkm');

    Route::get('statistika-pendaftaran/detail/{id}/show',[\App\Http\Controllers\Layouts\RegisController::class,'show'])->name('pendaftaran.show');

    Route::resource('faqs', \App\Http\Controllers\Layouts\FaqController::class);
    Route::get('faqs/{id}/destroy', [\App\Http\Controllers\Layouts\FaqController::class,'destroy'])->name('faqsDestroy');

    Route::resource('questions', \App\Http\Controllers\Layouts\QuestionController::class);
    Route::get('questions/{id}/destroy', [\App\Http\Controllers\Layouts\QuestionController::class,'destroy'])->name('questionsDestroy');

    Route::resource('suggestions', \App\Http\Controllers\Layouts\SuggestionController::class);
    Route::get('suggestions/{id}/destroy', [\App\Http\Controllers\Layouts\SuggestionController::class,'destroy'])->name('suggestionsDestroy');

});

Route::group(['prefix'=>'admin','as'=>'admin.','middleware'=>'guest'], function(){
    Route::get('/login', [\App\Http\Controllers\Auth\AuthController::class,'showFormLogin'])->name('formLogin');
    Route::post('/login/action',[\App\Http\Controllers\Auth\AuthController::class,'login'])->name('login');
    Route::get('/regis', [\App\Http\Controllers\Auth\AuthController::class,'showFormRegister'])->name('formRegister');
    Route::post('/regis/action',[\App\Http\Controllers\Auth\AuthController::class,'register'])->name('register');
    Route::post('/logout',[\App\Http\Controllers\Auth\AuthController::class,'logout'])->name('logout')->withoutMiddleware('guest');
});
