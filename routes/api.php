<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix'=>'v1','as'=>'client.','middleware'=>['token', 'cors']], function(){
    Route::get('categoriesnav',[\App\Http\Controllers\ApiV1\CategoryController::class,'categoriesnav'])->name('categories');
    Route::get('categories',[\App\Http\Controllers\ApiV1\CategoryController::class,'categories'])->name('categories');

//    Route::get('ukm/categories/{id}',[\App\Http\Controllers\ApiV1\CategoryController::class,'category'])->name('filterCategories');
    Route::get('ukm/detail/{slug}',[\App\Http\Controllers\ApiV1\UkmController::class,'detail'])->name('detailUkm');

    Route::post('ukm/regis/{id}',[\App\Http\Controllers\ApiV1\RegisController::class,'regis'])->name('regis');

    Route::post('ukm/questions/{id}',[\App\Http\Controllers\ApiV1\QuestionController::class,'create'])->name('questions.create');

    Route::get('carousels/',[\App\Http\Controllers\ApiV1\UkmController::class,'carousels'])->name('carousels');
    Route::get('search/{name}',[\App\Http\Controllers\ApiV1\UkmController::class,'search'])->name('search');

    Route::get('/tag',[\App\Http\Controllers\ApiV1\TagController::class,'index']);

    Route::post('/select/tags',[\App\Http\Controllers\ApiV1\RecomController::class,'selectTags'])->name('selectTags');
    Route::get('/recom',[\App\Http\Controllers\ApiV1\RecomController::class,'index'])->where('slug', '^[\w-]+$')->name('index');

    Route::post('/suggestions',[\App\Http\Controllers\ApiV1\SuggestionController::class,'index'])->name('suggestions');

});

